import {
  setComplaintDb,
  setCommentComplaintDb,
  getCommentsComplaintsDb,
  getDiscussionsComplaintsDb,
  deleteComplaintDb,
} from '../models/complaintsQueries'

export async function setComplaint(req, res) {
  try {
    const { discussionId, text } = req.body

    if (req.session.user_id) {
      const dbRes = await setComplaintDb(
        req.session.user_id,
        discussionId,
        text
      )

      if (dbRes) {
        res.status(200).json({ status: 0, message: 'Жалоба оставлена' })
      } else {
        res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
      }
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function setCommentComplaint(req, res) {
  try {
    const { commentId, text } = req.body

    if (req.session.user_id) {
      const dbRes = await setCommentComplaintDb(
        req.session.user_id,
        commentId,
        text
      )

      if (dbRes) {
        res.status(200).json({ status: 0, message: 'Жалоба оставлена' })
      } else {
        res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
      }
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function getAllComplaints(req, res) {
  try {
    const dbResDiscussions = await getDiscussionsComplaintsDb()

    if (dbResDiscussions) {
      const dbResComments = await getCommentsComplaintsDb()

      if (dbResComments) {
        res.status(200).json({
          status: 0,
          message: 'Найден список жалоб на обсуждения и комментарии',
          resultData: {
            discussions: dbResDiscussions,
            comments: dbResComments,
          },
        })
      } else {
        res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
      }

      return
    }

    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function deleteComplaint(req, res) {
  try {
    const { complaintId, commentId } = req.body
    const dbRes = await deleteComplaintDb(complaintId, commentId)

    if (dbRes) {
      res.status(200).json({ status: 0, message: 'Жалоба удалена' })
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
