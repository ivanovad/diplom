import {
  getCommentsDb,
  setCommentDb,
  deleteCommentDb,
} from '../models/commentsQueries'

/**
 * Обработка запроса на получение комментариев
 * @param req - объект, содержащий информацию о запросе
 * @param res - объект для отпраки ответа с сервера
 */
export async function getComments(req, res) {
  try {
    const { discussionId } = req.query
    const comments = await getCommentsDb(discussionId)

    if (comments) {
      if (comments.length === 0) {
        res
          .status(200)
          .json({ status: 1, message: 'Никто ещё не оставил комментарии' })
      } else {
        res.status(200).json({
          status: 0,
          message: 'Комментарии найдены',
          resultData: comments,
        })
      }
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
/**
 * Обработка запроса на добавление комментария
 * @param req - объект, содержащий информацию о запросе
 * @param res - объект для отпраки ответа с сервера
 */
export async function setComment(req, res) {
  try {
    const { discussionId, comment } = req.body

    /** Если есть userd_id в сессии */
    if (req.session.user_id) {
      const dbRes = await setCommentDb(
        discussionId,
        req.session.user_id,
        comment
      )

      if (dbRes === true) {
        res.status(200).json({ status: 0, message: 'Комментарий добавлен' })
      } else {
        if (dbRes) {
          res.status(200).json(dbRes)
        } else {
          res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
        }
      }
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
/**
 * Обработка запроса на удаление комментария
 * @param req - объект, содержащий информацию о запросе
 * @param res - объект для отпраки ответа с сервера
 */
export async function deleteComment(req, res) {
  try {
    const { commentId } = req.body
    const dbRes = await deleteCommentDb(commentId)

    if (dbRes) {
      res.status(200).json({ status: 0, message: 'Комментарий удалён' })
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
