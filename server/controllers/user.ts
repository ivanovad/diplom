import { Request, Response } from 'express'
import { getHash } from '../models/crypt'
import {
  addUser,
  findUser,
  findUserById,
  findUserByEmail,
  userBlockDb,
  getUserBlockDb,
  addAdmin,
} from '../models/userQueries'

export const userRegister = async (req, res) => {
  try {
    if (!Object.keys(req.body).length) {
      throw new Error('empty object')
    }

    const resObj = await regUser(req.body)

    res.status(201).json(resObj)
  } catch (error) {
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

async function regUser({
  email,
  password,
  name,
  surname,
}: {
  email: string
  password: string
  name: string
  surname: string
}) {
  try {
    if (password.length < 6) {
      return { status: 1, message: 'Слишком короткий пароль' }
    }

    const user = await findUserByEmail(email)

    if (user.length !== 0) {
      return {
        status: 2,
        message: 'Пользователь с данным email уже существует',
      }
    }

    const passwordHash = getHash(email, password)

    const addRes = await addUser(email, passwordHash, name, surname)

    if (addRes) {
      return { status: 0, message: 'Пользователь добавлен' }
    } else {
      return { status: -1, message: 'Ошибка в базе данных' }
    }
  } catch (error) {
    return { status: -1, message: 'Ошибка на сервере' }
  }
}

export const userCurrent = async (req, res: Response) => {
  try {
    if (req.session.user_id && req.session.type) {
      const user = await findUserById(req.session.user_id)

      if (user.length === 0) {
        res.status(404).json({ status: -1, message: 'Неверные cookie' })
        return
      }

      if (user === 'Произошла ошибка') {
        res.status(500).json({ status: -1, message: 'Ошибка в базе данных' })
        return
      }

      const jsonObj = {
        email: user[0].email,
        user_id: user[0].user_id,
        name: user[0].name,
        surname: user[0].surname,
        type: user[0].type,
      }

      res.status(200).json(jsonObj)
      return
    }

    res.status(200).json({ status: false })
  } catch (error) {
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export function userSession(req, res) {
  try {
    if (req.session.user_id && req.session.type) {
      res.status(200).json({
        status: 0,
        message: 'Пользователь найден',
        resultData: { userId: req.session.user_id, userType: req.session.type },
      })
    } else {
      res
        .status(200)
        .json({ status: 1, message: 'Пользователь не авторизован' })
    }
  } catch (error) {
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function userLogin(req, res) {
  try {
    const resObj = await authUser(req)

    res.status(200).json(resObj)
  } catch (error) {
    res.status(500).json({ message: 'Ошибка на сервере' })
  }
}

async function authUser(req) {
  try {
    const { email, password } = req.body
    const passwordHash = getHash(email, password)

    const result = await findUser(email, passwordHash)

    if (result.length !== 0) {
      req.session.user_id = result[0].user_id
      req.session.type = result[0].type

      return {
        status: 0,
        message: 'Пользователь найден',
        result: result[0].user_id,
      }
    }

    return { status: 1, message: 'Пользователь не найден' }
  } catch (error) {
    return { status: -1, message: 'Ошибка при поиске в базе данных' }
  }
}

export async function userLogout(req, res) {
  try {
    if (req.session.user_id) {
      req.session.destroy(() => false)

      res.status(200).json({ status: 0 })

      return
    }

    res.status(200).json({ status: -1, message: 'Ошибка на сервере' })
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function userBlock(req, res) {
  try {
    const { userId, reason } = req.body

    const dbRes = await userBlockDb(userId, reason)

    if (dbRes === true) {
      res.status(200).json({ status: 0, message: 'Пользователь заблокирован' })
    } else if (dbRes === false) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      res.status(200).json(dbRes)
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function getUserBlock(req, res) {
  try {
    const { userId } = req.query

    const dbRes = await getUserBlockDb(userId)

    if (dbRes.length === 0) {
      res
        .status(200)
        .json({ status: 0, message: 'Пользователь не заблокирован' })
    } else if (!dbRes) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      req.session.destroy(() => false)
      res.status(200).json({
        status: 1,
        message: 'Пользователь заблокирован',
        resultData: dbRes[0],
      })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function adminRegister(req, res) {
  try {
    const { email, password, name, surname } = req.body

    if (password.length < 6) {
      res.status(200).json({ status: 1, message: 'Слишком короткий пароль' })
      return
    }

    const user = await findUserByEmail(email)

    if (user.length !== 0) {
      res.status(200).json({
        status: 2,
        message: 'Пользователь с данным email уже существует',
      })
      return
    }

    const passwordHash = getHash(email, password)

    const addRes = await addAdmin(email, passwordHash, name, surname)

    if (addRes) {
      res.status(200).json({ status: 0, message: 'Администратор добавлен' })
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
