import {
  getPostsDb,
  setDiscussionDb,
  findLocationTitleDb,
  getMarkersDb,
  getDiscussionDb,
  getUserPostsDb,
  endDiscussionDb,
  getEndedPostsDb,
  findDiscussionsDb,
  deleteDiscussionDb,
} from '../models/discussionsQueries'

export async function allDiscussions(req, res) {
  try {
    let allPosts
    const { userOnly } = req.query

    if (userOnly === 'true') {
      if (req.session.user_id) {
        allPosts = await getUserPostsDb(req.session.user_id)
      } else {
        res
          .status(401)
          .json({ status: -1, message: 'Пользователь не авторизован' })
        return
      }
    } else {
      allPosts = await getPostsDb()
    }

    if (allPosts.length === 0) {
      res
        .status(200)
        .json({ status: 1, message: 'Пока нет ни одного обсуждения' })
    } else {
      res.status(200).json({
        status: 0,
        message: 'Обсуждения найдены',
        resultData: allPosts,
      })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function setDiscussion(req, res) {
  try {
    if (req.session.user_id) {
      const { coords, title, description, location } = req.body

      const findRes = await findLocationTitleDb(
        req.session.user_id,
        title,
        location
      )

      if (findRes.length !== 0) {
        res.status(200).json({
          status: 1,
          message: 'Вы уже создали метку по данному адресу с таким заголовком',
        })
      } else {
        const dbRes = await setDiscussionDb(
          req.session.user_id,
          coords,
          title,
          description,
          location
        )

        if (dbRes) {
          if (dbRes === true) {
            res.status(200).json({ status: 0, message: 'Обсуждение добавлено' })
          } else {
            res.status(200).json(dbRes)
          }
        } else {
          res
            .status(500)
            .json({ status: -1, message: 'Ошибка при добавлении обсуждения' })
        }
      }
    } else {
      res
        .status(401)
        .json({ status: -1, message: 'Пользователь не авторизован' })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function allMarkers(req, res) {
  try {
    const allMarkers = await getMarkersDb()

    if (allMarkers.length === 0) {
      res.status(200).json({ status: 1, message: 'Пока нет ни одной метки' })
    } else if (!allMarkers) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      res.status(200).json({
        status: 0,
        message: 'Маркеры найдены',
        resultData: allMarkers,
      })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function oneDiscussion(req, res) {
  try {
    const { discussionId } = req.query
    const discussion = await getDiscussionDb(discussionId)

    if (discussion.length === 0) {
      res
        .status(200)
        .json({ status: 1, message: 'Такого обсуждения не существует' })
    } else if (!discussion) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      res.status(200).json({
        status: 0,
        message: 'Обсуждение найдено',
        resultData: discussion[0],
      })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function endDiscussion(req, res) {
  try {
    const { discussionId } = req.body
    const dbRes = await endDiscussionDb(discussionId)

    if (dbRes) {
      res.status(200).json({ status: 0, message: 'Обсуждение завершено' })
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка в базе данных' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function allEndedDiscussions(req, res) {
  try {
    const allPosts = await getEndedPostsDb()

    if (allPosts.length === 0) {
      res.status(200).json({
        status: 1,
        message: 'Пока нет ни одного завершённого обсуждения',
      })
    } else if (!allPosts) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      res.status(200).json({
        status: 0,
        message: 'Обсуждения найдены',
        resultData: allPosts,
      })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function deleteDiscussion(req, res) {
  try {
    const { discussionId } = req.body
    const dbRes = await deleteDiscussionDb(discussionId)

    if (dbRes) {
      res.status(200).json({ status: 0, message: 'Обсуждение удалено' })
    } else {
      res.status(500).json({ status: -1, message: 'Ошибка в базе данных' })
    }

    return
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}

export async function findDiscussions(req, res) {
  try {
    const { value } = req.body
    const discussions = await findDiscussionsDb(value)

    if (discussions.length === 0) {
      res.status(200).json({
        status: 1,
        message: 'По Вашему запросу обсуждений не найдено',
      })
    } else if (!discussions) {
      res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
    } else {
      res.status(200).json({
        status: 0,
        message: 'Обсуждения найдены',
        resultData: discussions,
      })
    }
  } catch (error) {
    console.error(error)
    res.status(500).json({ status: -1, message: 'Ошибка на сервере' })
  }
}
