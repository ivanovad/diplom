import bodyParser from 'body-parser'
import {
  setComplaint,
  setCommentComplaint,
  getAllComplaints,
  deleteComplaint,
} from '../controllers/complaints'
import { Router } from 'express'

export const router = Router()

const urlEncodedParser = bodyParser.json()

router.post('/discussion', urlEncodedParser, setComplaint)
router.post('/comment', urlEncodedParser, setCommentComplaint)
router.get('/getAll', getAllComplaints)
router.delete('/delete', urlEncodedParser, deleteComplaint)

export default router
