import bodyParser from 'body-parser'
import { getComments, setComment, deleteComment } from '../controllers/comments'
import { Router } from 'express'

export const router = Router()

const urlEncodedParser = bodyParser.json()

/** Доступные роуты и запросы для комментариев */
router.get('/all', getComments)
router.post('/setComment', urlEncodedParser, setComment)
router.delete('/delete', urlEncodedParser, deleteComment)

export default router
