import bodyParser from 'body-parser'
import {
  allDiscussions,
  setDiscussion,
  allMarkers,
  oneDiscussion,
  endDiscussion,
  allEndedDiscussions,
  findDiscussions,
  deleteDiscussion,
} from '../controllers/discussions'
import { Router } from 'express'

export const router = Router()

const urlEncodedParser = bodyParser.json()
/** Доступные роуты и запросы для обсуждений */
router.get('/all', allDiscussions)
router.get('/archive', allEndedDiscussions)
router.get('/one', oneDiscussion)
router.get('/markers', allMarkers)
router.post('/setDiscussion', urlEncodedParser, setDiscussion)
router.post('/end', urlEncodedParser, endDiscussion)
router.post('/find', urlEncodedParser, findDiscussions)
router.delete('/delete', urlEncodedParser, deleteDiscussion)

export default router
