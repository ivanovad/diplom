import bodyParser from 'body-parser'
import { Router } from 'express'
import {
  userRegister,
  userLogin,
  userCurrent,
  userLogout,
  userSession,
  userBlock,
  getUserBlock,
  adminRegister,
} from '../controllers/user'

export const router = Router()

const urlEncodedParser = bodyParser.json()

router.get('/current', userCurrent)
router.get('/session', userSession)
router.get('/getBlock', getUserBlock)
router.delete('/logout', userLogout)
router.post('/registration', urlEncodedParser, userRegister)
router.post('/login', urlEncodedParser, userLogin)
router.post('/block', urlEncodedParser, userBlock)
router.post('/admin', urlEncodedParser, adminRegister)

export default router
