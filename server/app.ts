import express from 'express'
import cors from 'cors'
import bodyParser from 'body-parser'
import userRoutes from './routes/userRoutes'
import discussionsRoutes from './routes/discussionsRoutes'
import commentsRoutes from './routes/commentsRoutes'
import complaintsRoutes from './routes/complaintsRoutes'
import cookieParser from 'cookie-parser'
import session from 'express-session'

const app = express()

const pgp = require('pg-promise')()
const databaseConfig = {
  host: 'localhost',
  port: 8000,
  database: 'diplom',
  schema: 'citizen',
  user: 'postgres',
  password: '46893053',
}

export const db = pgp(databaseConfig)

const pgSession = require('connect-pg-simple')(session)

const corsOptions = {
  origin: 'http://localhost:3000',
  methods: 'GET, POST, DELETE',
  credentials: true,
  allowedHeaders: 'Content-Type, Authorization, X-Requested-With, Accept',
}

app.use(cors(corsOptions))
app.use(cookieParser())
app.use(
  session({
    secret: '$2b$10$OZMYvwW61J9pwLQSeQznlO',
    resave: false,
    saveUninitialized: false,
    cookie: {
      path: '/',
      maxAge: 30 * 24 * 60 * 60 * 1000, // 30 дней
      secure: false,
      httpOnly: true,
    },
    store: new pgSession({
      pgPromise: db,
      schemaName: 'citizen',
      tableName: 'session',
    }),
  })
)

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/user', userRoutes)
app.use('/discussions', discussionsRoutes)
app.use('/comments', commentsRoutes)
app.use('/complaints', complaintsRoutes)

app.listen(3001)
