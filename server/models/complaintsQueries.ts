import { ParameterizedQuery } from 'pg-promise'
import { db } from '../app'

export async function setComplaintDb(userId, discussionId, text) {
  const setComplaint = new ParameterizedQuery({
    text: `INSERT INTO citizen.discussions_complaints(discussion_id, user_id, reason) VALUES ($1, $2, $3)`,
    values: [discussionId, userId, text],
  })
  try {
    await db.none(setComplaint)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function setCommentComplaintDb(userId, commentId, text) {
  const setCommentComplaint = new ParameterizedQuery({
    text: `INSERT INTO citizen.comments_complaints(comment_id, user_id, reason) VALUES ($1, $2, $3)`,
    values: [commentId, userId, text],
  })
  try {
    await db.none(setCommentComplaint)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getCommentsComplaintsDb() {
  const getCommentsComplaints = new ParameterizedQuery({
    text: `SELECT a.*, b.email, b.name, b.surname, 
            c.user_id as authorId, c.content, c.discussion_id,
            d.email as authorEmail, d.name as authorName, d.surname as authorSurname
            FROM citizen.comments_complaints a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.comments c on a.comment_id=c.comment_id
            JOIN citizen.users d on d.user_id=c.user_id`,
  })

  try {
    const result = await db.any(getCommentsComplaints)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getDiscussionsComplaintsDb() {
  const getDiscussionsComplaints = new ParameterizedQuery({
    text: `SELECT a.*, b.email, b.name, b.surname, 
            c.user_id as authorId, c.title, c.discussion_text, c.date_end,
            d.email as authorEmail, d.name as authorName, d.surname as authorSurname
            FROM citizen.discussions_complaints a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.discussions c on a.discussion_id=c.discussion_id
            JOIN citizen.users d on d.user_id=c.user_id`,
  })

  try {
    const result = await db.any(getDiscussionsComplaints)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function deleteComplaintDb(complaintId, commentId) {
  let deleteComplaint
  if (commentId) {
    deleteComplaint = new ParameterizedQuery({
      text: `DELETE FROM citizen.comments_complaints WHERE complaint_id=$1`,
      values: [complaintId],
    })
  } else {
    deleteComplaint = new ParameterizedQuery({
      text: `DELETE FROM citizen.discussions_complaints WHERE complaint_id=$1`,
      values: [complaintId],
    })
  }

  try {
    await db.none(deleteComplaint)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}
