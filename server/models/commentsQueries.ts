import { ParameterizedQuery } from 'pg-promise'
import { db } from '../app'

/**
 * Выполнение запроса в бд на получение комментариев
 * @param discussionId - id обсуждения, комментарии которого получаем
 * @returns result | false - массив комментариев или false как сообщение об ошибке
 */
export async function getCommentsDb(discussionId: number) {
  const getComments = new ParameterizedQuery({
    text: `SELECT a.*, b.email, b.name, b.surname
            FROM citizen.comments a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.discussions c on c.discussion_id=a.discussion_id
            WHERE a.discussion_id=$1
            ORDER BY a.date DESC`,
    values: [discussionId],
  })

  try {
    const result = await db.any(getComments)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}
/**
 * Выполнение запроса к бд на добавление комментария
 * @param discussionId - id обсуждения, куда добавляем комментарий
 * @param userId - id пользователя, который добавляет комментарий
 * @param content - содержимое комментария
 * @returns true | false | {} - при успешном выполнении true иначе false или объект с
 * сообщением об ошибке
 */
export async function setCommentDb(
  discussionId: number,
  userId: number,
  content: string
) {
  const findComment = new ParameterizedQuery({
    text: `SELECT comment_id FROM citizen.comments 
           WHERE user_id=$1 AND discussion_id=$2 AND content=$3`,
    values: [userId, discussionId, content],
  })
  try {
    const res = await db.any(findComment)

    if (res.length === 0) {
      const setComment = new ParameterizedQuery({
        text: `INSERT INTO citizen.comments(user_id, discussion_id, content) VALUES ($1, $2, $3)`,
        values: [userId, discussionId, content],
      })

      await db.none(setComment)

      return true
    }

    return {
      status: -1,
      message: 'Вы уже оставили комментарий с таким содержимым',
    }
  } catch (error) {
    console.log(error)
    return false
  }
}
/**
 * Выполнение запроса к бд на удаление комментария
 * @param commentId - id удаляемого комментария
 * @returns true | false - успешное или неуспешное удаление
 */
export async function deleteCommentDb(commentId: number) {
  const deleteComment = new ParameterizedQuery({
    text: `DELETE FROM citizen.comments WHERE comment_id=$1`,
    values: [commentId],
  })
  try {
    await db.none(deleteComment)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}
