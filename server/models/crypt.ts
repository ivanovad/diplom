import bcrypt from 'bcrypt'

const salt = '$2b$10$JzUS0JATEwT14oS6zEtsb.'

export function getHash(email: string, password: string) {
  const passwordHash = bcrypt.hashSync(email + password, salt)

  return passwordHash
}
