import { ParameterizedQuery } from 'pg-promise'
import { db } from '../app'

export async function getPostsDb() {
  const getPosts = new ParameterizedQuery({
    text: `SELECT a.*, b.*, c.object
            FROM citizen.discussions a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.coords_objects c on c.coords_id=a.coords_id
            WHERE a.date_end IS NULL
            ORDER BY a.date_start DESC`,
  })

  try {
    const result = await db.any(getPosts)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getEndedPostsDb() {
  const getEndedPosts = new ParameterizedQuery({
    text: `SELECT a.*, b.*, c.object
            FROM citizen.discussions a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.coords_objects c on c.coords_id=a.coords_id
            WHERE a.date_end IS NOT NULL
            ORDER BY a.date_start DESC`,
  })

  try {
    const result = await db.any(getEndedPosts)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getUserPostsDb(userId) {
  const getPosts = new ParameterizedQuery({
    text: `SELECT a.*, b.email, b.name, b.surname, c.object
            FROM citizen.discussions a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.coords_objects c on c.coords_id=a.coords_id
            WHERE a.user_id=$1
            ORDER BY a.date_end DESC, a.date_start DESC`,
    values: [userId],
  })

  try {
    const result = await db.any(getPosts)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function findCoordsDb(coords) {
  const coordsId = new ParameterizedQuery({
    text: 'SELECT coords_id FROM citizen.discussions_coords WHERE latitude=$1 and longitude=$2',
    values: [Number(coords.lat), Number(coords.lng)],
  })

  try {
    const result = await db.any(coordsId)

    return result
  } catch (error) {
    console.log('Поиск координат', error)

    return false
  }
}

export async function findLocationDb(coordsId, location) {
  const findLocation = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.coords_objects WHERE coords_id=$1 and object=$2',
    values: [coordsId, location],
  })

  try {
    const result = await db.any(findLocation)

    return result
  } catch (error) {
    console.log('Поиск локации', error)

    return false
  }
}

export async function findLocationTitleDb(userId, title, location) {
  const findTitleLocation = new ParameterizedQuery({
    text: `SELECT * FROM citizen.discussions WHERE user_id=$1 
      and title=$2 and coords_id in 
      (SELECT coords_id FROM citizen.coords_objects
        WHERE object=$3)`,
    values: [userId, title, location],
  })

  try {
    const result = await db.any(findTitleLocation)

    return result
  } catch (error) {
    console.log('Поиск локации', error)

    return false
  }
}

async function setLocationDb(coords, location) {
  try {
    const coordsId = await findCoordsDb(coords)

    if (!coordsId || coordsId.length === 0) {
      throw new Error('Ошибка при поиске координат')
    }

    const result = await findLocationDb(coordsId[0].coords_id, location)

    if (result.length !== 0) {
      return true
    }

    const setLocation = new ParameterizedQuery({
      text: 'INSERT INTO citizen.coords_objects(coords_id, object) VALUES ($1, $2)',
      values: [coordsId[0].coords_id, location],
    })

    await db.none(setLocation)

    return true
  } catch (error) {
    console.log('Добавление локации', error)

    return false
  }
}

async function setCoordsDb(coords) {
  try {
    const coordsId = await findCoordsDb(coords)

    if (coordsId.length !== 0) {
      return true
    }

    const setCoords = new ParameterizedQuery({
      text: 'INSERT INTO citizen.discussions_coords(latitude, longitude) VALUES ($1, $2)',
      values: [coords.lat, coords.lng],
    })

    await db.none(setCoords)

    return true
  } catch (error) {
    console.log('Добавление координат ', error)

    return false
  }
}

export async function setDiscussionDb(
  userId,
  coords,
  title,
  description,
  location
) {
  try {
    const resCoordAdd = await setCoordsDb(coords)

    if (!resCoordAdd) {
      throw new Error('Ошибка при добавлении координат')
    }

    const resLocationAdd = await setLocationDb(coords, location)

    if (!resLocationAdd) {
      throw new Error('Ошибка при добавлении адреса')
    }

    const coordsId = await findCoordsDb(coords)

    if (!coordsId || coordsId.length === 0) {
      throw new Error('Ошибка при поиске координат')
    }

    const setDiscussion = new ParameterizedQuery({
      text: 'INSERT INTO citizen.discussions(user_id, coords_id, title, discussion_text) VALUES ($1, $2, $3, $4)',
      values: [userId, coordsId[0].coords_id, title, description],
    })

    await db.none(setDiscussion)

    return true
  } catch (error) {
    console.log(error)
    if (error.constraint === 'discussions_user_id_coords_id_title_key') {
      return {
        status: 1,
        message: 'Вы уже создали метку по данному адресу с таким заголовком',
      }
    }

    return false
  }
}

export async function getMarkersDb() {
  const getMarkers = new ParameterizedQuery({
    text: `SELECT a.latitude, a.longitude, b.discussion_id, b.title, b.date_start, b.date_end 
            FROM citizen.discussions_coords a
            JOIN citizen.discussions b on a.coords_id=b.coords_id`,
  })

  try {
    const result = await db.any(getMarkers)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getDiscussionDb(discussionId) {
  const getDiscussion = new ParameterizedQuery({
    text: `SELECT a.*, b.email, b.name, b.surname, c.object
            FROM citizen.discussions a
            JOIN citizen.users b on a.user_id=b.user_id
            JOIN citizen.coords_objects c on c.coords_id=a.coords_id
            WHERE a.discussion_id=$1`,
    values: [discussionId],
  })

  try {
    const result = await db.any(getDiscussion)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function endDiscussionDb(discussionId) {
  const endDiscussion = new ParameterizedQuery({
    text: `UPDATE citizen.discussions SET date_end=LOCALTIMESTAMP(0)
            WHERE discussion_id=$1`,
    values: [discussionId],
  })

  try {
    await db.none(endDiscussion)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function deleteDiscussionDb(discussionId) {
  const deleteDiscussion = new ParameterizedQuery({
    text: `DELETE FROM citizen.discussions
            WHERE discussion_id=$1`,
    values: [discussionId],
  })

  try {
    await db.none(deleteDiscussion)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function findDiscussionsDb(value) {
  const filter = '%' + value + '%'
  const getDiscussion = new ParameterizedQuery({
    text: `SELECT a.*, b.object, c.*
           FROM citizen.discussions a
           JOIN citizen.coords_objects b on a.coords_id=b.coords_id
           JOIN citizen.users c on a.user_id=c.user_id
           WHERE lower(a.title) LIKE $1 OR lower(a.discussion_text) LIKE $1 OR lower(b.object) LIKE $1
           ORDER BY a.date_end DESC, a.date_start DESC`,
    values: [filter.toLowerCase()],
  })

  try {
    const result = await db.any(getDiscussion)

    return result
  } catch (error) {
    console.log(error)

    return false
  }
}
