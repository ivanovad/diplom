import { ParameterizedQuery } from 'pg-promise'
import { db } from '../app'

export async function addUser(
  email: string,
  passwordHash: string,
  name: string,
  surname: string
) {
  const addUser = new ParameterizedQuery({
    text: 'INSERT INTO citizen.users(email, password, name, surname) VALUES ($1, $2, $3, $4)',
    values: [email, passwordHash, name, surname],
  })

  try {
    await db.none(addUser)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function findUser(email: string, passwordHash: string) {
  const findUser = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.users WHERE email=$1 and password=$2',
    values: [email, passwordHash],
  })

  try {
    const response = await db.any(findUser)

    return response
  } catch (error) {
    console.log(error)

    Promise.reject('Произошла ошибка')
  }
}

export async function findUserById(id: number) {
  const findUserById = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.users WHERE user_id=$1',
    values: [id],
  })

  try {
    const response = await db.any(findUserById)

    return response
  } catch (error) {
    console.log(error)

    Promise.reject('Произошла ошибка')
  }
}

export async function findUserByEmail(email: string) {
  const findUser = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.users WHERE email=$1',
    values: [email],
  })

  try {
    const response = await db.any(findUser)

    return response
  } catch (error) {
    console.log(error)

    Promise.reject('Произошла ошибка')
  }
}

export async function userBlockDb(userId: number, reason: string) {
  const findUserById = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.users_blocks WHERE user_id=$1',
    values: [userId],
  })

  try {
    const result = await db.any(findUserById)

    if (result.length !== 0) {
      return { status: -1, message: 'Пользователь уже заблокирован' }
    }

    const findUser = new ParameterizedQuery({
      text: 'INSERT INTO citizen.users_blocks(user_id, reason) VALUES ($1, $2)',
      values: [userId, reason],
    })

    await db.none(findUser)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function getUserBlockDb(userId: number) {
  const getUserBlocks = new ParameterizedQuery({
    text: 'SELECT * FROM citizen.users_blocks WHERE user_id=$1',
    values: [userId],
  })

  try {
    const response = await db.any(getUserBlocks)

    return response
  } catch (error) {
    console.log(error)

    return false
  }
}

export async function addAdmin(
  email: string,
  passwordHash: string,
  name: string,
  surname: string
) {
  const addUser = new ParameterizedQuery({
    text: 'INSERT INTO citizen.users(email, password, name, surname, type) VALUES ($1, $2, $3, $4, $5)',
    values: [email, passwordHash, name, surname, 'admin'],
  })

  try {
    await db.none(addUser)

    return true
  } catch (error) {
    console.log(error)

    return false
  }
}
