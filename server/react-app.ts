import express from 'express'
import path from 'path'

const app = express()

app.use(express.static(path.join(__dirname, '../client/build')))
app.use(express.static('public'))

app.use((req, res, next) => {
  res.sendFile(path.join(__dirname, '../client/build', 'index.html'))
})

app.listen(3000)
