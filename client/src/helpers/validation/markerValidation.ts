export function markerError(value, setErrors) {
  let errorText: null | string = null

  if (value.lat === 0 && value.lng === 0) {
    errorText = 'Не выбрана точка на карте'
  }

  setErrors((prevState) => ({
    ...prevState,
    markerError: errorText,
  }))

  return errorText
}
