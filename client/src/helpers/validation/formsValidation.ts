export function isEmpty(value) {
  let noSpaceText = null
  if (value) {
    noSpaceText = getNoExtraSpacesText(value)
  }

  return noSpaceText ? false : true
}

export function getNoExtraSpacesText(value) {
  if (value) {
    return value.trim()
  }

  return null
}

export function deleteAllSpaces(value) {
  return getNoExtraSpacesText(value).replace(/\s+/g, '')
}

export function setEmailErrors(value, setError) {
  let errorText: null | string = null

  if (isEmpty(value)) {
    errorText = 'Email не может быть пустым'
  } else if (isCorrectEmail(value)) {
    errorText = 'Некорректный Email'
  }

  setError((prevState) => ({
    ...prevState,
    emailError: errorText,
  }))

  return errorText
}

export function isCorrectEmail(value) {
  let reg = /.+@.+\..+/i

  return !reg.test(value)
}

export function setPasswordErrors(value, setError) {
  let errorText: null | string = null

  if (isEmpty(value)) {
    errorText = 'Пароль не может быть пустым'
  }

  setError((prevState) => ({
    ...prevState,
    passwordError: errorText,
  }))

  return errorText
}

export function setPasswordRepeatErrors(value, passwordValue, setError) {
  let errorText: null | string = null

  if (isEmpty(value)) {
    errorText = 'Поле не может быть пустым'
  } else if (!isSamePasswords(passwordValue, value)) {
    errorText = 'Пароли не совпадают'
  }

  setError((prevState) => ({
    ...prevState,
    passwordRepeatError: errorText,
  }))

  return errorText
}

export function isSamePasswords(password, passwordRepeat) {
  return password === passwordRepeat
}

export function isAllNumbers(value) {
  let reg = /\d/

  return reg.test(value)
}

export function noFirstZerosValue(value) {
  return value === '' ? '' : String(Number(value))
}

export function setTitleErrors(value, setError) {
  let errorText: null | string = null

  if (isEmpty(value)) {
    errorText = 'Заголовок не может быть пустым'
  }

  setError((prevState) => ({
    ...prevState,
    titleError: errorText,
  }))

  return errorText
}
