import { getNoExtraSpacesText } from './validation/formsValidation'

export function cutAdress(value) {
  return value.split(',').slice(0, -3).join(',')
}

export function getName(email, name, surname) {
  if (getNoExtraSpacesText(name) && getNoExtraSpacesText(surname)) {
    return `${surname} ${name}`
  }

  if (getNoExtraSpacesText(name)) {
    return name
  }

  return email.split('@')[0]
}
