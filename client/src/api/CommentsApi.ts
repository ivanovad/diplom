class CommentsApi {
  baseUrl = 'http://localhost:3001/comments'
  /**
   * Выполнение запроса на получение списка комментариев
   * @param discussionId - id обсуждения, комментарии к которому получаем
   * @returns resJon - список комментариев
   */
  async getComments(discussionId: number) {
    try {
      const response = await fetch(
        `${this.baseUrl}/all?discussionId=${discussionId}`,
        {
          method: 'GET',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get comments: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении комментариев',
      }
    }
  }
  /**
   * Выполнение запроса на добавление нового комментария
   * @param discussionId - id обсуждения, куда добавляем комментарий
   * @param comment - содержимое добавляемого комментария
   * @returns resJson - ответ сервера об успешности добавления
   */
  async setComment(discussionId: number, comment: string) {
    try {
      const response = await fetch(`${this.baseUrl}/setComment`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ discussionId, comment }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in set comment: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при добавлении комментария',
      }
    }
  }
  /**
   * Выполнение запроса на удаление комментария
   * @param commentId - id удаляемого комментария
   * @returns resJson - ответ сервера об успешности удаления
   */
  async deleteComment(commentId: number) {
    try {
      const response = await fetch(`${this.baseUrl}/delete`, {
        method: 'DELETE',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ commentId }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in delete comment: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при удалении комментария',
      }
    }
  }
}

export default new CommentsApi()
