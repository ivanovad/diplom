class UserApi {
  baseUrl = 'http://localhost:3001/user'

  async addUser(data: {
    email: string
    password: string
    name: string
    surname: string
  }) {
    try {
      const response = await fetch(`${this.baseUrl}/registration`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in addUser: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при добавлении пользователя',
      }
    }
  }

  async registration(
    email: string,
    password: string,
    name: string,
    surname: string
  ) {
    try {
      const resp = await this.addUser({ email, password, name, surname })

      return resp
    } catch (error) {
      console.log(`Error in registration: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при регистрации',
      }
    }
  }

  async getUser() {
    try {
      const response = await fetch(`${this.baseUrl}/current`, {
        method: 'GET',
        credentials: 'include',
      })

      const currentUser = await response.json()

      return currentUser
    } catch (error) {
      console.log(`Error in get user: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении пользователя',
      }
    }
  }

  async getSessionUser() {
    try {
      const response = await fetch(`${this.baseUrl}/session`, {
        method: 'GET',
        credentials: 'include',
      })

      const currentUser = await response.json()

      return currentUser
    } catch (error) {
      console.log(`Error in get session user: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении пользователя',
      }
    }
  }

  async authorization(email: string, password: string) {
    try {
      const response = await fetch(`${this.baseUrl}/login`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password }),
      })

      const dataJson = await response.json()

      return dataJson
    } catch (error) {
      console.log(`Error in authorization: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при авторизации',
      }
    }
  }

  async logout() {
    try {
      const response = await fetch(`${this.baseUrl}/logout`, {
        method: 'DELETE',
        headers: {
          'Content-type': 'application/json',
        },
        credentials: 'include',
      })

      const dataJson = await response.json()

      return dataJson
    } catch (error) {
      console.error('Error in logout', error)

      return {
        status: -1,
        message: 'Произошла ошибка при выходе',
      }
    }
  }

  async blockUser(userId, reason) {
    try {
      const response = await fetch(`${this.baseUrl}/block`, {
        method: 'POST',
        headers: {
          'Content-type': 'application/json',
        },
        credentials: 'include',
        body: JSON.stringify({ userId, reason }),
      })

      const dataJson = await response.json()

      return dataJson
    } catch (error) {
      console.error('Error in block user', error)

      return {
        status: -1,
        message: 'Произошла ошибка при блокировке пользователя',
      }
    }
  }

  async checkBlocks(userId) {
    try {
      const response = await fetch(
        `${this.baseUrl}/getBlock?userId=${userId}`,
        {
          method: 'GET',
          headers: {
            'Content-type': 'application/json',
          },
          credentials: 'include',
        }
      )

      const dataJson = await response.json()

      return dataJson
    } catch (error) {
      console.error('Error in get user block', error)

      return {
        status: -1,
        message: 'Произошла ошибка при проверке блокировки пользователя',
      }
    }
  }

  async addAdmin(
    email: string,
    password: string,
    name: string,
    surname: string
  ) {
    try {
      const response = await fetch(`${this.baseUrl}/admin`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ email, password, name, surname }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in add admin: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при добавлении администратора',
      }
    }
  }
}

export default new UserApi()
