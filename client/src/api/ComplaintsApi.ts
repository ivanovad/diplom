class ComplaintsApi {
  baseUrl = 'http://localhost:3001/complaints'

  async setDiscussionComplaint(discussionId, text) {
    try {
      const response = await fetch(`${this.baseUrl}/discussion`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ discussionId, text }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in set discussion complaint: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка оставлении жалобы',
      }
    }
  }

  async setCommentComplaint(commentId, text) {
    try {
      const response = await fetch(`${this.baseUrl}/comment`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ commentId, text }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in set comment complaint: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка оставлении жалобы',
      }
    }
  }

  async getAllComplaints() {
    try {
      const response = await fetch(`${this.baseUrl}/getAll`, {
        method: 'GET',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get all complaints: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении всех жалоб',
      }
    }
  }

  async deleteComplaint(complaintId, commentId) {
    try {
      const response = await fetch(`${this.baseUrl}/delete`, {
        method: 'DELETE',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ complaintId, commentId }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in delete complaint: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при удалении жалобы',
      }
    }
  }
}

export default new ComplaintsApi()
