import { OpenStreetMapProvider } from 'leaflet-geosearch'

class DiscussionsApi {
  baseUrl = 'http://localhost:3001/discussions'

  async getPosts(userOnly = false) {
    try {
      const response = await fetch(`${this.baseUrl}/all?userOnly=${userOnly}`, {
        method: 'GET',
        credentials: 'include',
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get posts: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении обсуждений',
      }
    }
  }

  async getEndedPosts() {
    try {
      const response = await fetch(`${this.baseUrl}/archive`, {
        method: 'GET',
        credentials: 'include',
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get archive posts: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении обсуждений',
      }
    }
  }

  async getMarkers() {
    try {
      const response = await fetch(`${this.baseUrl}/markers`)

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get markers: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении меток',
      }
    }
  }

  async getLocation(coords) {
    try {
      const provider = new OpenStreetMapProvider()
      const locationString = await provider.search({
        query: `[${coords.lat}, ${coords.lng}]`,
      })

      return locationString
    } catch (error) {
      console.log(`Error in get location: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении адреса',
      }
    }
  }

  async setDiscussion(coords, title, description) {
    try {
      const locationString = await this.getLocation(coords)

      const response = await fetch(`${this.baseUrl}/setDiscussion`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          coords,
          title,
          description,
          location: locationString[0].label,
        }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in set marker: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при добавлении метки',
      }
    }
  }

  async getDiscussion(discussionId) {
    try {
      const response = await fetch(
        `${this.baseUrl}/one?discussionId=${discussionId}`,
        {
          method: 'GET',
          credentials: 'include',
          headers: {
            'Content-Type': 'application/json',
          },
        }
      )

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in get post: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при получении обсуждения',
      }
    }
  }

  async endDiscussion(discussionId) {
    try {
      const response = await fetch(`${this.baseUrl}/end`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ discussionId }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in end post: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при завершении обсуждения',
      }
    }
  }

  async deleteDiscussion(discussionId) {
    try {
      const response = await fetch(`${this.baseUrl}/delete`, {
        method: 'DELETE',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ discussionId }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in delete post: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при удалении обсуждения',
      }
    }
  }

  async findPosts(value) {
    try {
      const response = await fetch(`${this.baseUrl}/find`, {
        method: 'POST',
        credentials: 'include',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({ value }),
      })

      const resJson = await response.json()

      return resJson
    } catch (error) {
      console.log(`Error in find posts: ${error}`)

      return {
        status: -1,
        message: 'Произошла ошибка при поиске обсуждений',
      }
    }
  }
}

export default new DiscussionsApi()
