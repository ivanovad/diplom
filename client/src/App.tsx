import './styles/App.css'
import React, { useState, useEffect } from 'react'
import Header from './components/Header'
import MainPage from './components/MainPage'
import { Switch, Route, Redirect } from 'react-router-dom'
import Registration from './components/Registration'
import Login from './components/Login'
import Api from './api/UserApi'

function App() {
  const [isAuth, setAuth] = useState(false)
  const [isLoading, setLoading] = useState(true)
  const [user, setUser] = useState({
    email: '',
    user_id: -1,
    name: '',
    surname: '',
    type: '',
  })

  useEffect(() => {
    async function getUser() {
      const result = await Api.getUser()

      if (result.user_id) {
        setAuth(true)
        setUser(result)
      }

      setLoading(false)
    }

    getUser()
  }, [])

  return (
    <div className="App">
      <Header
        isAuth={isAuth}
        isLoading={isLoading}
        setAuth={setAuth}
        user={user}
        setUser={setUser}
      />
      <Switch>
        <Route path="/registration">
          {isAuth ? <Redirect exact to="/" /> : <Registration />}
        </Route>
        <Route path="/login">
          {isAuth ? (
            <Redirect exact to="/" />
          ) : (
            <Login setAuth={setAuth} setUser={setUser} />
          )}
        </Route>
        <Route path="/">
          <MainPage isAuth={isAuth} user={user} />
        </Route>
      </Switch>
    </div>
  )
}

export default App
