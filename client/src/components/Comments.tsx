import React, { useEffect, useState } from 'react'
import Api from '../api/CommentsApi'
import UserApi from '../api/UserApi'
import { commentStyles } from '../components/materialComponents/styles/comments'
import Alerts from './materialComponents/Alerts/Alerts'
import CommentCard from './materialComponents/Card/CommentCard'
import CommentInput from './materialComponents/Inputs/CommentInput'

interface Props {
  discussion: number
  isAuth: boolean
  userType: string
}

type CommentData = {
  comment_id: number
  email: string
  name: string
  surname: string
  content: string
  date: string
  user_id: number
}

function Comments(props: Props) {
  const [response, setResponse] = useState('')
  const [open, setOpen] = useState(false)
  const [comments, setComments] = useState<CommentData[]>([])
  const [isLoading, setIsLoading] = useState(true)
  const [userId, setUserId] = useState(null)

  async function getComments() {
    const result = await Api.getComments(props.discussion)

    if (result.status === 0) {
      setComments(result.resultData)
    } else if (result.status === -1) {
      setOpen(true)
    }

    setResponse(result.message)
    setIsLoading(false)
  }

  async function getUserId() {
    const result = await UserApi.getSessionUser()

    if (result.status === 0) {
      setUserId(result.resultData.userId)
    }
  }

  useEffect(() => {
    getComments()
    getUserId()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    const intervalId = setInterval(() => {
      getComments()
      getUserId()
    }, 1500)
    return () => clearInterval(intervalId)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const classes = commentStyles()
  return (
    <div>
      <div className={classes.commentBlockTitle}>Комментарии</div>

      <div className={classes.commentBlock}>
        {props.isAuth ? (
          <CommentInput discussionId={props.discussion} />
        ) : (
          <div className={classes.noAuth}>
            Авторизуйтесь, чтобы оставлять комментарии
          </div>
        )}

        {!isLoading && (
          <>
            {response === 'Никто ещё не оставил комментарии' ? (
              <div className={classes.noComments}>{response}</div>
            ) : (
              <div>
                {comments.map((comment) => (
                  <CommentCard
                    key={comment.comment_id}
                    userEmail={comment.email}
                    name={comment.name}
                    surname={comment.surname}
                    text={comment.content}
                    date={comment.date}
                    isAuth={props.isAuth}
                    commentUserId={comment.user_id}
                    curUserId={userId}
                    commentId={comment.comment_id}
                    userType={props.userType}
                  />
                ))}
              </div>
            )}
          </>
        )}
      </div>

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="comments"
        response={response}
      />
    </div>
  )
}

export default Comments
