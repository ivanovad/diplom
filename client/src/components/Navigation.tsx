import React, { useState, useEffect } from 'react'
import { Drawer } from '@material-ui/core'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import IconButton from '@material-ui/core/IconButton'
import { Link } from 'react-router-dom'
import MapIcon from '../images/map.svg'
import DiscussIcon from '../images/horn.svg'
import MyDisussIcon from '../images/new.svg'
import ArchiveIcon from '../images/inbox-alt.svg'
import ComplaintsIcon from '../images/chat-warning.svg'
import AddAdminIcon from '../images/person-add.svg'
import useStyles from './materialComponents/styles/drawer'
import openMenu from '../images/openMenu.svg'
import closeMenu from '../images/closeMenu.svg'
import cx from 'classnames'

interface Props {
  isAuth: boolean
  open: boolean
  userType: string
}

export default function Navigation(props: Props) {
  const [open, setOpen] = useState(true)
  const icons = [DiscussIcon, MapIcon, MyDisussIcon, ArchiveIcon]
  const links = ['/discussions', '/map', '/my-posts', '/archive']
  const names = ['Обсуждения', 'Карта', 'Мои обсуждения', 'Архив']

  if (props.userType === 'admin') {
    icons.push(ComplaintsIcon, AddAdminIcon)
    links.push('/complaints', '/admins-add')
    names.push('Жалобы', 'Добавление модераторов')
  }

  useEffect(() => {}, [props.isAuth])

  useEffect(() => {
    setOpen(props.open)
  }, [props.open])

  const classes = useStyles()

  function handleDrawerOpen() {
    setOpen(!open)
  }

  return (
    <div className={classes.container}>
      <div className={classes.div}>
        <IconButton
          aria-label="open drawer"
          onClick={handleDrawerOpen}
          className={classes.root}
          edge="end"
        >
          {open ? (
            <img src={closeMenu} alt="close menu" />
          ) : (
            <img src={openMenu} alt="open menu" />
          )}
        </IconButton>
      </div>
      <Drawer
        className={cx(classes.drawer, {
          [classes.drawerClosed]: !open,
        })}
        classes={{
          paper: classes.paperAnchorDockedLeft,
        }}
        variant="persistent"
        open={open}
        anchor="left"
      >
        <List>
          {names.map((text, index) => (
            <Link to={links[index]} className={classes.link} key={text}>
              <ListItem button>
                <ListItemIcon>
                  {<img src={icons[index]} alt={text + 'icon'} />}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
            </Link>
          ))}
        </List>
      </Drawer>
    </div>
  )
}
