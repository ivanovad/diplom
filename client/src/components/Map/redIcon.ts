import * as L from 'leaflet'
import markerImg from '../../images/marker-icon-red.png'

export const redIcon = new L.Icon({
  iconUrl: markerImg,
  iconSize: [25, 41],
  iconAnchor: [12, 41],
  popupAnchor: [1, -34],
})
