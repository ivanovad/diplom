import React, { useState, useEffect } from 'react'
import { MapAddProps } from './types'
import RefreshButton from '../materialComponents/Buttons/RefreshButton'
import CustomButton from '../materialComponents/Buttons/CustomButton'
import Paper from '@material-ui/core/Paper'
import CustomInput from '../materialComponents/Inputs/CustomInput'
import * as validation from '../../helpers/validation/formsValidation'
import * as markerValidation from '../../helpers/validation/markerValidation'
import Alerts from '../materialComponents/Alerts/Alerts'
import '../../styles/App.css'
import Api from '../../api/DiscussionsApi'
import cx from 'classnames'

function MapAdd(props: MapAddProps) {
  const [open, setOpen] = useState(false)
  const [response, setResponseMessage] = useState('')
  const [buttonValue, setButtonValue] = useState('Добавить метку')
  const [errors, setErrors] = useState({ titleError: null, markerError: null })

  useEffect(() => {}, [props.isAuth])

  function handleShowClick() {
    props.setIsSecondary(!props.isSecondary)
    buttonValue === 'Добавить метку'
      ? setButtonValue('Отмена')
      : setButtonValue('Добавить метку')
  }

  function handleChange(event) {
    props.setValues({ ...props.values, [event.target.id]: event.target.value })
  }

  async function handleAddButtonClick() {
    if (!checkErrors()) {
      const result = await Api.setDiscussion(
        props.values.marker,
        props.values.title,
        props.values.text
      )

      if (result.status === 0) {
        setTimeout(() => window.location.reload(), 2500)
      }

      setResponseMessage(result.message)
      setOpen(true)
    }
  }

  function checkErrors() {
    const titleError = validation.setTitleErrors(props.values.title, setErrors)
    const markerError = markerValidation.markerError(
      props.values.marker,
      setErrors
    )

    if (markerError) {
      setResponseMessage(markerError)
      setOpen(true)
    }

    return Boolean(titleError || markerError)
  }

  return (
    <div className="map-add">
      <RefreshButton />
      {props.isAuth ? (
        <>
          <CustomButton
            margin
            value={buttonValue}
            onClick={handleShowClick}
            isSecondary={props.isSecondary}
          />
          <div
            className={cx('map-helper', {
              'map-none': !props.isSecondary,
            })}
          >
            Кликните на любое место на карте, чтобы оставить метку. Для отмены
            добавления нажмите "ОТМЕНА"
          </div>

          <Paper
            elevation={2}
            className={cx('map-form', { 'map-none': !props.isSecondary })}
          >
            <CustomInput
              id="title"
              label="Заголовок метки"
              required
              value={props.values.title}
              handleChange={handleChange}
              limit={30}
              error={errors.titleError}
              map
            />

            <CustomInput
              id="text"
              label="Описание"
              multiline
              value={props.values.text}
              handleChange={handleChange}
              limit={300}
              map
            />

            <CustomButton
              value="Добавить"
              mapForm
              onClick={handleAddButtonClick}
            />
          </Paper>
        </>
      ) : (
        <div className="map-auth">Авторизуйтесь, чтобы добавлять метки</div>
      )}

      <Alerts
        open={open}
        setOpen={setOpen}
        response={response}
        autoHideDuration={3000}
        type="map"
      />
    </div>
  )
}

export default MapAdd
