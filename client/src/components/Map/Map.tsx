import React, { useState, useEffect, useRef } from 'react'
import { Map as MapContainer, TileLayer, Marker, Popup } from 'react-leaflet'
import { Link, useLocation } from 'react-router-dom'
import '../../styles/App.css'
import Api from '../../api/DiscussionsApi'
import Alerts from '../materialComponents/Alerts/Alerts'
import { redIcon } from './redIcon'
import { getTimeString } from '../../helpers/dateTime'
import MarkerClusterGroup from 'react-leaflet-markercluster'
import { MapProps, MarkerData } from './types'
import MapAdd from './MapAdd'

/**
 * Получение объета с GET-параметрами текущего URL
 * @returns URLSearchParams object
 */
const useQuery = () => new URLSearchParams(useLocation().search)

export default function Map(props: MapProps) {
  /** Состояние, показывающее открыт ли блок добавления метки*/
  const [isSecondary, setIsSecondary] = useState(false)
  const [values, setValues] = useState({
    title: '',
    text: '',
    marker: { lat: 0, lng: 0 },
  })
  /** Состояние для открытия информационных сообщений */
  const [open, setOpen] = useState(false)
  const [response, setResponseMessage] = useState('')
  const [markers, setMarkers] = useState<MarkerData[]>([])

  const mapRef = useRef(null)
  //Получение GET параметров текущего URL
  const query = useQuery()
  //Получение значения id обсуждения из параметров
  const queryDiscussionParam = Number(query.get('discussion'))

  /** Получение списка маркеров с сервера через api */
  async function getAllMarkers() {
    const result = await Api.getMarkers()

    if (result.status !== 0) {
      setOpen(true)
      setResponseMessage(result.message)
    } else {
      setMarkers(result.resultData)
    }
  }

  /** Получение всех марекров при mount компонента */
  useEffect(() => {
    getAllMarkers()
  }, [])

  /**
   * Получение марекров каждые 30 секунд
   * и очищение подписки на обновление при unmount компонента
   */
  useEffect(() => {
    const intervalId = setInterval(() => getAllMarkers(), 30000)
    return () => {
      clearInterval(intervalId)
    }
  }, [])

  /** Ререндер компонента когда меняется isAuth */
  useEffect(() => {}, [props.isAuth])

  /**
   * Добавление маркера по клику на карту
   * @param event - событие по клику
   */
  function addMarker(event: any) {
    if (isSecondary) {
      setValues({ ...values, marker: event.latlng })
    }
  }

  /**
   * Автоматиеское открытие popup маркера и
   * премещение центра карты в координаты маркера,
   * если есть GET-параметр id обсуждения
   * @param ref - реф маркера
   */
  const initMarker = (ref) => {
    if (ref) {
      ref.leafletElement.openPopup()

      mapRef.current &&
        mapRef.current.leafletElement.panTo(ref.leafletElement.getLatLng())
    }
  }

  return (
    <div className="map-page">
      <MapContainer
        center={[55.028191, 82.9211489]}
        zoom={12}
        onClick={addMarker}
        ref={mapRef}
      >
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
        />
        <MarkerClusterGroup
          key={`group-${markers.length}`}
          showCoverageOnHover={false}
          maxClusterRadius={50}
        >
          {markers.map(
            (marker) =>
              !marker.date_end && (
                <Marker
                  ref={
                    queryDiscussionParam === marker.discussion_id
                      ? initMarker
                      : null
                  }
                  key={`marker-${marker.discussion_id}`}
                  position={{ lat: marker.latitude, lng: marker.longitude }}
                >
                  <Popup>
                    <div className="popup-time">
                      {getTimeString(marker.date_start)}
                    </div>
                    <div className="popup-title">{marker.title}</div>
                    <Link to={`/discussions/${marker.discussion_id}`}>
                      Обсуждение
                    </Link>
                  </Popup>
                </Marker>
              )
          )}
        </MarkerClusterGroup>
        {isSecondary && (
          <Marker key="marker-new" icon={redIcon} position={values.marker} />
        )}
      </MapContainer>

      <MapAdd
        isAuth={props.isAuth}
        isSecondary={isSecondary}
        setIsSecondary={setIsSecondary}
        values={values}
        setValues={setValues}
      />

      <Alerts
        open={open}
        setOpen={setOpen}
        response={response}
        autoHideDuration={3000}
        type="map"
      />
    </div>
  )
}
