export interface MapProps {
  isAuth: boolean
}

export interface MapAddProps {
  isAuth: boolean
  isSecondary: boolean
  setIsSecondary: (isSecondary: boolean) => void
  values: {
    title: string
    text: string
    marker: {
      lat: number
      lng: number
    }
  }
  setValues: React.Dispatch<
    React.SetStateAction<{
      title: string
      text: string
      marker: {
        lat: number
        lng: number
      }
    }>
  >
}

export type MarkerData = {
  latitude: number
  longitude: number
  title: string
  discussion_id: number
  date_start: string
  date_end: string
}
