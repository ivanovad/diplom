import React, { useState, useEffect } from 'react'
import RefreshButton from './materialComponents/Buttons/RefreshButton'
import CustomSelect from './materialComponents/Select/CustomSelect'
import Api from '../api/ComplaintsApi'
import Alerts from '../components/materialComponents/Alerts/Alerts'
import '../styles/App.css'
import ComplaintCard from './materialComponents/Card/ComplaintCard'

function Complaints() {
  const [complaints, setComplaints] = useState({
    discussions: [],
    comments: [],
  })
  const [isLoading, setIsLoading] = useState(true)
  const [type, setType] = useState('all')
  const [open, setOpen] = useState(false)
  const [response, setResponse] = useState('')

  useEffect(() => {
    getAllComplaints()
  }, [])

  async function getAllComplaints() {
    setIsLoading(true)
    const result = await Api.getAllComplaints()

    if (result.status === -1) {
      setResponse(result.message)
      setOpen(true)
    } else {
      setComplaints(result.resultData)
    }

    setIsLoading(false)
  }

  return (
    <div>
      <div className="complaint-header">
        <CustomSelect setType={setType} />
        <RefreshButton />
      </div>

      {type === 'all' &&
        !isLoading &&
        complaints.comments.length === 0 &&
        complaints.discussions.length === 0 && (
          <div className="not-content">Никто не оставил жалоб</div>
        )}

      {type !== 'all' && !isLoading && complaints[type].length === 0 && (
        <div className="not-content">Никто не оставил жалоб данного типа</div>
      )}

      {type !== 'all' && !isLoading && complaints[type].length !== 0 && (
        <>
          {complaints[type].map((complaint) => (
            <ComplaintCard
              key={complaint.complaint_id}
              type={type}
              complaint={complaint}
            />
          ))}
        </>
      )}

      {type === 'all' && !isLoading && (
        <>
          {complaints['discussions'].map((complaint) => (
            <ComplaintCard
              key={complaint.complaint_id + complaint.discussion_id}
              type={type}
              complaint={complaint}
            />
          ))}
          {complaints['comments'].map((complaint) => (
            <ComplaintCard
              key={complaint.complaint_id}
              type={type}
              complaint={complaint}
            />
          ))}
        </>
      )}

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="complaints"
        response={response}
      />
    </div>
  )
}

export default Complaints
