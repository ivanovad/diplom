import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import Api from '../api/DiscussionsApi'
import UserApi from '../api/UserApi'
import Comments from './Comments'
import Alerts from './materialComponents/Alerts/Alerts'
import CardDiscussion from './materialComponents/Card/CardDiscussion'

interface RouteParams {
  discussionId: string | undefined
}

type PostData = {
  title: string
  date_start: string
  date_end: string | null
  discussion_id: number
  discussion_text: string | null
  email: string
  name: string | null
  surname: string | null
  user_id: number
  coords_id: number
  object: string
}

interface Props {
  isAuth: boolean
  userType: string
}

function Post(props: Props) {
  let { discussionId } = useParams<RouteParams>()
  const [post, setPost] = useState<PostData>({
    title: '',
    date_start: '',
    date_end: null,
    discussion_id: 0,
    discussion_text: null,
    email: '',
    name: null,
    surname: null,
    user_id: 0,
    coords_id: 0,
    object: '',
  })
  const [response, setResponse] = useState('')
  const [open, setOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [curUserId, setCurUserId] = useState(null)

  useEffect(() => {
    async function getDiscussion() {
      const result = await Api.getDiscussion(Number(discussionId))

      if (result.status === 0) {
        setPost(result.resultData)
      } else if (result.status === 1) {
        setResponse(result.message)
      } else {
        setResponse(result.message)
        setOpen(true)
      }

      setIsLoading(false)
    }

    async function getUserId() {
      const result = await UserApi.getSessionUser()

      if (result.status === 0) {
        setCurUserId(result.resultData.userId)
      }
    }

    getDiscussion()
    getUserId()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {}, [props.isAuth])

  return (
    <>
      {!isLoading && (
        <div>
          {response === 'Такого обсуждения не существует' ? (
            <div>{response}</div>
          ) : (
            <>
              <CardDiscussion
                key={`post-${post.discussion_id}`}
                title={post.title}
                content={post.discussion_text}
                userEmail={post.email}
                dateStart={post.date_start}
                dateEnd={post.date_end}
                name={post.name}
                surname={post.surname}
                location={post.object}
                discussionId={post.discussion_id}
                userId={post.user_id}
                curUserId={curUserId}
                isAuth={props.isAuth}
                userType={props.userType}
                postPage
              />

              <Comments
                isAuth={props.isAuth}
                discussion={post.discussion_id}
                userType={props.userType}
              />
            </>
          )}

          <Alerts
            open={open}
            autoHideDuration={5000}
            setOpen={setOpen}
            type="post"
            response={response}
          />
        </div>
      )}
    </>
  )
}

export default Post
