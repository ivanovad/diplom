import React from 'react'
import Logo from '../images/logo.svg'
import '../styles/App.css'

function WelcomePage() {
  return (
    <div className="welcome-page">
      <div className="welcome-title">
        Добро пожаловать в приложение Горожанин!
      </div>
      <div className="welcome-content">
        Отмечайте на карте события, произошедшие в городе, и обсуждайте их с
        другими горожанами
      </div>
      <img src={Logo} alt="logo" />
    </div>
  )
}

export default WelcomePage
