import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Api from '../api/UserApi'
import { getName } from '../helpers/splitFunctions'
import Logo from '../images/logo.svg'
import userIcon from '../images/person.svg'
import Alerts from './materialComponents/Alerts/Alerts'
import CustomButton from './materialComponents/Buttons/CustomButton'
import LogoutButton from './materialComponents/Buttons/LogoutButton'

interface Props {
  isAuth: boolean
  isLoading: boolean
  setAuth: (a: boolean) => void
  setUser: (user) => void
  user: {
    email: string
    user_id: number
    name: string
    surname: string
    type: string
  }
}

export default function Header(props: Props) {
  const [response, setResponse] = useState('')
  const [open, setOpen] = useState(false)

  async function handleLogout() {
    const result = await Api.logout()

    if (result.status === 0) {
      props.setAuth(false)
      props.setUser({ email: '', user_id: -1, name: '', surname: '', type: '' })
    } else {
      setResponse(result.message)
      setOpen(true)
    }
  }

  return (
    <div className="header">
      <Link to="/">
        <div className="logo">
          <img src={Logo} alt="logo" />
          <div>Горожанин</div>
        </div>
      </Link>

      {!props.isLoading ? (
        props.isAuth ? (
          <div className="header__entry">
            <div className="header__entry__personal__container">
              <img src={userIcon} alt="user icon" />
              <div className="header__entry__personal">
                {getName(props.user.email, props.user.name, props.user.surname)}
              </div>
            </div>
            <LogoutButton onClick={handleLogout} header value="Выход" />
          </div>
        ) : (
          <div className="header__entry">
            <Link to="/login">
              <CustomButton margin value="Вход" />
            </Link>
            <Link to="/registration">
              <CustomButton margin value="Регистрация" />
            </Link>
          </div>
        )
      ) : null}

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="header"
        response={response}
      />
    </div>
  )
}
