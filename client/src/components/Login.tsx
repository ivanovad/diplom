import React, { ChangeEvent, useState } from 'react'
import PasswordInput from './materialComponents/Inputs/PasswordInput'
import CustomButton from './materialComponents/Buttons/CustomButton'
import Api from '../api/UserApi'
import CustomInput from './materialComponents/Inputs/CustomInput'
import { formStyles } from './materialComponents/styles/inputs'
import Link from '@material-ui/core/Link'
import * as validation from '../helpers/validation/formsValidation'
import Alerts from './materialComponents/Alerts/Alerts'
import BlockAlert from './materialComponents/Alerts/BlockAlert'

interface Props {
  setAuth: (isAuth: boolean) => void
  setUser: (user) => void
}

function Login(props: Props) {
  const classes = formStyles()

  let [values, setValues] = useState({
    email: '',
    password: '',
  })

  let [errors, setErrors] = useState({
    emailError: null,
    passwordError: null,
  })

  const [response, setResponseMessage] = useState('')
  const [reason, setReason] = useState('')
  const [open, setOpen] = useState(false)
  const [openBlock, setOpenBlock] = useState(false)

  async function handleSubmit(event) {
    event.preventDefault()

    if (!checkErrors()) {
      const result = await Api.authorization(values.email, values.password)
      setResponseMessage(result.message)

      if (result.status !== 0) {
        setOpen(true)
      } else {
        const userResult = await Api.getUser()

        if (userResult.message) {
          setResponseMessage(userResult.message)
          setOpen(true)
        } else {
          const blocksResult = await Api.checkBlocks(userResult.user_id)

          if (blocksResult.status) {
            setReason(blocksResult.resultData.reason)
            setOpenBlock(true)
          } else if (blocksResult.status === -1) {
            setResponseMessage(userResult.message)
            setOpen(true)
          } else {
            props.setAuth(true)
            props.setUser(userResult)
          }
        }
      }
    }
  }

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValues({ ...values, [event.target.id]: event.target.value })
  }

  function checkErrors() {
    const emailError = validation.setEmailErrors(values.email, setErrors)
    const passwordError = validation.setPasswordErrors(
      values.password,
      setErrors
    )

    return Boolean(emailError || passwordError)
  }

  return (
    <div className={classes.formContainer}>
      <form id="login-form" className={classes.form}>
        <div className={classes.title}>Вход</div>

        <CustomInput
          id="email"
          required
          label="E-mail"
          value={values.email}
          handleChange={handleChange}
          error={errors.emailError}
        />

        <PasswordInput
          label="Пароль"
          id="password"
          handleChange={handleChange}
          value={values.password}
          error={errors.passwordError}
        />

        <CustomButton fullWidth onClick={handleSubmit} value="Войти" />

        <Link className={classes.link} href="/registration">
          Нет аккаунта? Зарегистрируйтесь
        </Link>

        <BlockAlert
          openBlock={openBlock}
          setOpenBlock={setOpenBlock}
          reason={reason}
        />

        <Alerts
          open={open}
          setOpen={setOpen}
          response={response}
          autoHideDuration={6000}
          type="login"
        />
      </form>
    </div>
  )
}

export default Login
