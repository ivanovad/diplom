import React, { useState, ChangeEvent } from 'react'
import { useLocation } from 'react-router-dom'
import Api from '../api/UserApi'
import CustomButton from './materialComponents/Buttons/CustomButton'
import CustomInput from './materialComponents/Inputs/CustomInput'
import PasswordInput from './materialComponents/Inputs/PasswordInput'
import { formStyles } from './materialComponents/styles/inputs'
import Link from '@material-ui/core/Link'
import * as validation from '../helpers/validation/formsValidation'
import Alerts from './materialComponents/Alerts/Alerts'
import cx from 'classnames'

function Registration() {
  const currentPath = useLocation().pathname

  const [values, setValues] = useState({
    email: '',
    password: '',
    name: '',
    surname: '',
    passwordRepeat: '',
  })

  const [errors, setErrors] = useState({
    emailError: '',
    passwordError: '',
    passwordRepeatError: '',
  })

  const [response, setResponseMessage] = useState('')
  const [open, setOpen] = useState(false)
  const classes = formStyles()

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    setValues({ ...values, [event.target.id]: event.target.value })

    if (event.target.id === 'passwordRepeat') {
      validation.setPasswordRepeatErrors(
        event.target.value,
        values.password,
        setErrors
      )
    }
  }

  async function handleSubmit(event) {
    event.preventDefault()

    if (!checkErrors()) {
      if (currentPath === '/admins-add') {
        const result = await Api.addAdmin(
          validation.getNoExtraSpacesText(values.email),
          validation.getNoExtraSpacesText(values.password),
          validation.getNoExtraSpacesText(values.name),
          validation.getNoExtraSpacesText(values.surname)
        )

        setResponseMessage(result.message)
        setOpen(true)
      } else {
        const result = await Api.registration(
          validation.getNoExtraSpacesText(values.email),
          validation.getNoExtraSpacesText(values.password),
          validation.getNoExtraSpacesText(values.name),
          validation.getNoExtraSpacesText(values.surname)
        )

        setResponseMessage(result.message)
        setOpen(true)
      }
    }
  }

  function checkErrors() {
    const emailError = validation.setEmailErrors(values.email, setErrors)
    const passwordError = validation.setPasswordErrors(
      values.password,
      setErrors
    )
    const passwordRepeatError = validation.setPasswordRepeatErrors(
      values.passwordRepeat,
      values.password,
      setErrors
    )

    return Boolean(emailError || passwordError || passwordRepeatError)
  }

  return (
    <div
      className={cx(classes.formContainer, {
        [classes.formContainerAdmin]: currentPath === '/admins-add',
      })}
    >
      <form id="reg-form" className={classes.form}>
        {currentPath === '/registration' ? (
          <div className={classes.title}>Регистрация</div>
        ) : (
          <div className={classes.titleAdmin}>Добавление модератора</div>
        )}

        <CustomInput
          id="email"
          label="E-mail"
          handleChange={handleChange}
          required
          value={values.email}
          error={errors.emailError}
        />

        <CustomInput
          id="name"
          label="Имя"
          handleChange={handleChange}
          value={values.name}
          error={null}
        />

        <CustomInput
          id="surname"
          label="Фамилия"
          handleChange={handleChange}
          value={values.surname}
          error={null}
        />

        <PasswordInput
          id="password"
          label="Пароль"
          handleChange={handleChange}
          value={values.password}
          error={errors.passwordError}
        />

        <PasswordInput
          id="passwordRepeat"
          label="Повторите пароль"
          handleChange={handleChange}
          value={values.passwordRepeat}
          error={errors.passwordRepeatError}
        />

        {currentPath === '/registration' ? (
          <CustomButton
            fullWidth
            onClick={handleSubmit}
            value="Зарегистрироваться"
          />
        ) : (
          <CustomButton fullWidth onClick={handleSubmit} value="Добавить" />
        )}

        {currentPath === '/registration' && (
          <Link className={classes.link} href="/login">
            Уже есть аккаунт? Войдите
          </Link>
        )}

        <Alerts
          open={open}
          autoHideDuration={6000}
          setOpen={setOpen}
          type="register"
          response={response}
        />
      </form>
    </div>
  )
}

export default Registration
