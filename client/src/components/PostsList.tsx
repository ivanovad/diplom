import React, { useEffect, useState } from 'react'
import Button from '@material-ui/core/Button'
import Api from '../api/DiscussionsApi'
import UserApi from '../api/UserApi'
import Alerts from './materialComponents/Alerts/Alerts'
import RefreshButton from './materialComponents/Buttons/RefreshButton'
import CardDiscussion from './materialComponents/Card/CardDiscussion'
import SearchInput from './materialComponents/Inputs/SearchInput'
import { searchStyles } from './materialComponents/styles/searchInput'
import '../styles/App.css'
import cx from 'classnames'
import { buttonStyles } from './materialComponents/styles/buttons'

export type PostData = {
  title: string
  date_start: string
  date_end: string | null
  discussion_id: number
  discussion_text: string | null
  email: string
  name: string | null
  surname: string | null
  user_id: number
  coords_id: number
  object: string
}

interface Props {
  isAuth: boolean
  userType: string
}

function PostsList(props: Props) {
  const [postsList, setPostsList] = useState<PostData[] | undefined>([])
  const [response, setResponse] = useState('')
  const [open, setOpen] = useState(false)
  const [isLoading, setIsLoading] = useState(true)
  const [curUserId, setCurUserId] = useState(null)
  const [showMore, setShowMore] = useState(1)
  const [isSearch, setIsSearch] = useState(false)

  async function getAllPosts() {
    const result = await Api.getPosts()

    if (result.status === 0) {
      setPostsList(result.resultData)
    } else if (result.status !== 1) {
      setResponse(result.message)
      setOpen(true)
    }

    setIsLoading(false)
  }

  async function getUserId() {
    const result = await UserApi.getSessionUser()

    if (result.status === 0) {
      setCurUserId(result.resultData.userId)
    }
  }

  useEffect(() => {
    getAllPosts()
    getUserId()
  }, [])

  useEffect(() => {}, [props.isAuth])

  const classes = searchStyles()
  const buttonClasses = buttonStyles()

  return (
    <div className="post-page">
      <div className={cx(classes.searchLine, 'search')}>
        <SearchInput setPostsList={setPostsList} setIsSearch={setIsSearch} />
        <RefreshButton />
      </div>

      {!isLoading &&
        postsList &&
        (postsList.length === 0 ? (
          <div className="not-content">
            Пока не было создано ни одного обсуждения
          </div>
        ) : (
          <div>
            {postsList.slice(0, showMore * 4).map((post) => (
              <CardDiscussion
                key={`post-${post.discussion_id}`}
                title={post.title}
                content={post.discussion_text}
                userEmail={post.email}
                dateStart={post.date_start}
                dateEnd={post.date_end}
                name={post.name}
                surname={post.surname}
                location={post.object}
                discussionId={post.discussion_id}
                userId={post.user_id}
                curUserId={curUserId}
                isAuth={props.isAuth}
                userType={props.userType}
              />
            ))}
            {showMore * 4 <= postsList.length && (
              <Button
                fullWidth
                className={buttonClasses.primary}
                variant="contained"
                onClick={() => setShowMore(showMore + 1)}
              >
                Показать больше
              </Button>
            )}
          </div>
        ))}

      {isSearch && !postsList && (
        <div className="not-content">По Вашему запросу ничего не найдено</div>
      )}

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="posts"
        response={response}
      />
    </div>
  )
}

export default PostsList
