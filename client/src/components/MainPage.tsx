import React, { useRef, useEffect, useState } from 'react'
import Navigation from './Navigation'
import UserPosts from './UserPosts'
import PostsList from './PostsList'
import CreatePost from './CreatePost'
import Map from './Map/Map'
import { Switch, Route, Redirect } from 'react-router-dom'
import WelcomePage from './WelcomePage'
import Post from './Post'
import Archive from './Archive'
import Complaints from './Complaints'
import Registration from './Registration'

interface Props {
  isAuth: boolean
  user: {
    email: string
    user_id: number
    name: string
    surname: string
    type: string
  }
}

export default function MainPage(props: Props) {
  const divRef = useRef<HTMLDivElement | null>(null)
  const [open, setOpen] = useState(true)
  const [userType, setUserType] = useState('user')

  /** Закрытие меню, если размер экрана меньше 1100px при mount компонента */
  useEffect(() => {
    if (divRef.current && divRef.current.offsetWidth < 1100) {
      setOpen(false)
    }
  }, [])

  /** Утсановление типа пользователя при изменении isAuth или user */
  useEffect(() => {
    setUserType(props.user.type)
  }, [props.isAuth, props.user])

  return (
    <div ref={divRef} className="main-page">
      <Navigation isAuth={props.isAuth} open={open} userType={userType} />
      <div className="main-page__content">
        <Switch>
          <Route exact path="/">
            <WelcomePage />
          </Route>
          <Route exact path="/create-post">
            <CreatePost />
          </Route>
          <Route exact path="/map">
            <Map isAuth={props.isAuth} />
          </Route>
          <Route exact path="/my-posts">
            <UserPosts isAuth={props.isAuth} userType={props.user.type} />
          </Route>
          <Route exact path="/discussions">
            <PostsList isAuth={props.isAuth} userType={props.user.type} />
          </Route>
          <Route path="/discussions/:discussionId">
            <Post isAuth={props.isAuth} userType={props.user.type} />
          </Route>
          <Route exact path="/archive">
            <Archive isAuth={props.isAuth} userType={props.user.type} />
          </Route>
          {props.isAuth && (
            <Route exact path="/complaints">
              <Complaints />
            </Route>
          )}
          {props.isAuth && (
            <Route exact path="/admins-add">
              <Registration />
            </Route>
          )}
          {!props.isAuth && <Redirect exact to="/" />}
        </Switch>
      </div>
    </div>
  )
}
