import { makeStyles } from '@material-ui/core/styles'

export const selectStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 150,
    marginRight: theme.spacing(5),
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))
