import { makeStyles } from '@material-ui/core/styles'

export const cardStyles = makeStyles((theme) => ({
  root: {
    minWidth: 275,
    marginBottom: theme.spacing(3),
    backgroundColor: '#FCFCFC',
  },
  title: {
    color: '#757575',
    fontSize: '1rem',
    textAlign: 'right',
    paddingBottom: '0px !important',
  },
  titleEnd: {
    padding: '20px',
  },
  header: {
    padding: '0px !important',
  },
  date: {
    color: '#757575',
    fontSize: '12px',
    textAlign: 'right',
    marginLeft: '20px',
    marginBottom: '15px',
  },
  name: {
    marginBottom: '14px',
    color: '#1976D2',
    display: 'flex',
    alignItems: 'center',
  },
  img: {
    marginRight: '10px',
    width: '32px',
  },
  discussionTitle: {
    fontSize: '24px',
    lineHeight: '1.3',
    marginBottom: '10px',
    letterSpacing: '1.2px',
  },
  content: {
    marginBottom: '20px',
  },
  location: {
    fontStyle: 'italic',
    fontSize: '14px',
  },
  link: {
    color: 'black',
  },
  subheader: {
    color: 'black',
    fontStyle: 'italic',
  },
  contentEnd: {
    display: 'flex',
    justifyContent: 'space-between',
  },
  footer: {
    justifyContent: 'center',
  },
}))
