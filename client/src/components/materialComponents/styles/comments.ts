import { makeStyles } from '@material-ui/core/styles'

export const commentStyles = makeStyles((theme) => ({
  root: {
    padding: '10px 12px',
    display: 'flex',
    alignItems: 'center',
    border: '2px solid #757575',
    borderRadius: '10px',
  },
  commentBlockTitle: {
    fontSize: '20px',
    lineHeight: '1.3',
    marginBottom: '25px',
    letterSpacing: '1.2px',
  },
  commentBlock: {
    marginLeft: '30px',
  },
  noComments: {
    fontStyle: 'italic',
    marginBottom: '10px',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  img: {
    marginRight: '20px',
    color: '#FF9C0C',
  },
  container: {
    display: 'flex',
    alignItems: 'start',
    marginBottom: '20px',
  },
  textContainer: {
    width: '70%',
  },
  noAuth: {
    marginBottom: '20px',
    color: '#757575',
  },
  userComment: {
    border: '2px solid #757575',
    borderRadius: '10px',
    width: 'fit-content',
  },
  user: {
    display: 'flex',
    flexDirection: 'column',
    marginRight: '10px',
    alignItems: 'center',
    flexWrap: 'wrap',
    maxWidth: 'min-content',
    fontSize: '14px',
  },
  comment: {
    display: 'flex',
    maxWidth: '70%',
    marginBottom: '30px',
  },
  commentContent: {
    paddingTop: '0px',
  },
}))
