import { makeStyles } from '@material-ui/core/styles'

const drawerWidth = 240
const drawerWidthTwo = 60

const useStyles = makeStyles((theme) => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  drawerClosed: {
    width: drawerWidthTwo,
  },
  paperAnchorDockedLeft: {
    border: 'none',
    top: 'initial',
    paddingLeft: '30px',
    width: drawerWidth,
  },
  link: {
    textDecoration: 'none',
    color: 'black',
  },
  div: {
    display: 'flex',
    flexDirection: 'row-reverse',
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
  },
  root: {
    padding: '0px',
  },
}))

export default useStyles
