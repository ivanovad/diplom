import { makeStyles } from '@material-ui/core/styles'

export const formStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(1, 0),
  },
  form: {
    width: '400px',
    display: 'flex',
    flexDirection: 'column',
  },
  formContainer: {
    marginTop: theme.spacing(3),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  formContainerAdmin: {
    marginTop: 0,
  },
  title: {
    textAlign: 'center',
    marginBottom: theme.spacing(3),
    fontFamily: 'Cuprum',
    fontSize: '28px',
    fontWeight: 400,
    letterSpacing: '5px',
    textTransform: 'uppercase',
  },
  titleAdmin: {
    textAlign: 'center',
    marginBottom: theme.spacing(2),
    fontFamily: 'Cuprum',
    fontSize: '24px',
    fontWeight: 400,
    letterSpacing: '5px',
    textTransform: 'uppercase',
  },
  link: {
    alignSelf: 'center',
  },
  map: {
    margin: theme.spacing(2, 2, 0),
    width: '90%',
  },
}))
