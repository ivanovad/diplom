import { makeStyles } from '@material-ui/core/styles'

export const complaintStyles = makeStyles((theme) => ({
  card: {
    minWidth: 275,
    marginBottom: theme.spacing(3),
    backgroundColor: '#FCFCFC',
  },
  title: {
    color: '#757575',
    fontSize: '1rem',
    textAlign: 'right',
    paddingBottom: '0px',
  },
  userContainer: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(2),
  },
  userImg: {
    marginRight: theme.spacing(2),
  },
  userName: {
    color: '#1976D2',
    marginRight: theme.spacing(1),
  },
  link: {
    textAlign: 'center',
    color: 'black',
  },
  reason: {
    marginBottom: theme.spacing(2),
  },
  content: {
    display: 'flex',
    marginBottom: theme.spacing(2),
  },
  type: {
    fontStyle: 'italic',
    marginRight: theme.spacing(2),
  },
}))
