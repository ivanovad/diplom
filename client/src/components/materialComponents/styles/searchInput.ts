import { makeStyles } from '@material-ui/core/styles'

export const searchStyles = makeStyles((theme) => ({
  root: {
    padding: '2px 4px 0px',
    display: 'flex',
    alignItems: 'center',
    width: '70%',
    backgroundColor: '#FCFCFC',
    minWidth: '285px',
  },
  input: {
    marginLeft: theme.spacing(1),
    flex: 1,
  },
  iconButton: {
    padding: 10,
  },
  container: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    flexWrap: 'wrap',
  },
  searchLine: {
    display: 'flex',
    alignItems: 'center',
    marginBottom: theme.spacing(3),
  },
}))
