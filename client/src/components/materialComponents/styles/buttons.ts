import { withStyles } from '@material-ui/core/styles'
import Button from '@material-ui/core/Button'
import { makeStyles } from '@material-ui/core/styles'

export const ColorButton = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText('#1976D2'),
    backgroundColor: '#1976D2',
    '&:hover': {
      backgroundColor: '#115293',
    },
    margin: theme.spacing(3, 0, 2),
    letterSpacing: '1.5px',
  },
}))(Button)

export const buttonStyles = makeStyles((theme) => ({
  margin: {
    margin: theme.spacing(0, 2),
  },
  secondary: {
    color: theme.palette.getContrastText('#DC004E'),
    backgroundColor: '#DC004E',
    '&:hover': {
      backgroundColor: '#9A0036',
    },
  },
  primary: {
    color: theme.palette.getContrastText('#1976D2'),
    backgroundColor: '#1976D2',
    '&:hover': {
      backgroundColor: '#115293',
    },
  },
  mapForm: {
    margin: theme.spacing(0, 2, 1),
    width: '90%',
  },
}))

export const ColorButtonLogout = withStyles((theme) => ({
  root: {
    color: theme.palette.getContrastText('#DDDDDD'),
    backgroundColor: '#DDDDDD',
    '&:hover': {
      backgroundColor: '#BABABA',
    },
    margin: theme.spacing(0, 0, 0, 5),
    letterSpacing: '1.5px',
  },
}))(Button)

export const refreshButtonStyles = makeStyles((theme) => ({
  primary: {
    color: theme.palette.getContrastText('#1976D2'),
    backgroundColor: '#1976D2',
    '&:hover': {
      backgroundColor: '#115293',
    },
    margin: theme.spacing(3, 0, 2),
    letterSpacing: '1.5px',
  },
  container: {
    textAlign: 'right',
  },
}))
