import React from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import { getName } from '../../../helpers/splitFunctions'
import { Link } from 'react-router-dom'
import CardHeader from '@material-ui/core/CardHeader'
import userIcon from '../../../images/person.svg'
import CustomMenu from '../Menu/CustomMenu'
import { complaintStyles } from '../styles/complaint'

interface Props {
  type: string
  complaint: any
}

function ComplaintCard(props: Props) {
  const complaint = props.complaint
  const classes = complaintStyles()

  return (
    <div>
      <Card className={classes.card}>
        <CardHeader
          className={classes.title}
          disableTypography
          action={
            <CustomMenu
              iconType="24"
              type="complaint"
              userType="admin"
              commentId={complaint.comment_id}
              discussionId={complaint.discussion_id}
              complaintId={complaint.complaint_id}
              isEnd={Boolean(complaint.date_end)}
              authorId={complaint.authorid}
              userId={complaint.user_id}
            />
          }
        />
        <CardContent>
          <div className={classes.userContainer}>
            <img className={classes.userImg} src={userIcon} alt="user icon" />
            <div className={classes.userName}>
              {getName(complaint.email, complaint.name, complaint.surname)}
            </div>
            оставил жалобу:
          </div>
          <div className={classes.reason}>{complaint.reason}</div>
          {complaint.title && (
            <div className={classes.content}>
              <div className={classes.type}>Обсуждение</div>
              <div>
                <div>
                  {`Автор: ${getName(
                    complaint.authoremail,
                    complaint.authorname,
                    complaint.authorsurname
                  )}`}
                </div>
                <div>{`Заголовок: ${complaint.title}`}</div>
                <div>
                  {complaint.discussion_text !== '' &&
                    `Описание: ${complaint.discussion_text}`}
                </div>
              </div>
            </div>
          )}
          {complaint.comment_id && (
            <div className={classes.content}>
              <div className={classes.type}>Комментарий</div>
              <div>
                <div>
                  {`Автор: ${getName(
                    complaint.authoremail,
                    complaint.authorname,
                    complaint.authorsurname
                  )}`}
                </div>
                <div>{`Содержимое: ${complaint.content}`}</div>
              </div>
            </div>
          )}
          <div></div>
          <div></div>

          <Link
            className={classes.link}
            to={`/discussions/${complaint.discussion_id}`}
          >
            <div>Перейти к обсуждению</div>
          </Link>
        </CardContent>
      </Card>
    </div>
  )
}

export default ComplaintCard
