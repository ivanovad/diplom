import React, { useEffect } from 'react'
import Card from '@material-ui/core/Card'
import CardActions from '@material-ui/core/CardActions'
import CardContent from '@material-ui/core/CardContent'
import { cardStyles } from '../styles/card'
import { Link } from 'react-router-dom'
import { getTimeString } from '../../../helpers/dateTime'
import { cutAdress, getName } from '../../../helpers/splitFunctions'
import userIcon from '../../../images/person.svg'
import CustomMenu from '../Menu/CustomMenu'
import CardHeader from '@material-ui/core/CardHeader'
import cx from 'classnames'
import '../../../styles/App.css'

interface Props {
  isAuth: boolean
  title: string
  content: string | null
  userEmail: string
  dateStart: string
  dateEnd: string | null
  name: string | null
  surname: string | null
  location: string
  discussionId: number
  userId?: number
  curUserId?: number | null
  postPage?: boolean
  userType: string
}

function CardDiscussion(props: Props) {
  useEffect(() => {}, [props.isAuth])

  const classes = cardStyles()

  function isDiscussionOwner() {
    return props.userId === props.curUserId
  }

  function isEnd() {
    if (props.dateEnd) {
      return 'Обсуждение завершено'
    }

    return null
  }

  return (
    <>
      <Card className={classes.root}>
        <CardHeader
          className={cx(classes.title, {
            [classes.titleEnd]: props.dateEnd,
          })}
          disableTypography
          classes={{
            content: cx({ [classes.contentEnd]: props.dateEnd }),
          }}
          action={
            props.isAuth && (
              <CustomMenu
                iconType="32"
                type="discussion"
                isEnd={Boolean(isEnd())}
                discussionOwner={isDiscussionOwner()}
                discussionId={props.discussionId}
                userType={props.userType}
                userId={props.userId}
              />
            )
          }
          subheader={
            props.dateEnd && <div>{getTimeString(props.dateStart)}</div>
          }
          title={
            props.dateEnd ? (
              <div className={classes.subheader}>{isEnd()}</div>
            ) : (
              getTimeString(props.dateStart)
            )
          }
        />

        <CardContent>
          <div className={classes.name}>
            <img className={classes.img} src={userIcon} alt="user icon" />
            {isDiscussionOwner()
              ? 'Вы'
              : getName(props.userEmail, props.name, props.surname)}
          </div>

          <div className={classes.discussionTitle}>{props.title}</div>
          <div className={classes.content}>{props.content}</div>
          <div className={classes.location}>
            Адрес: {cutAdress(props.location)}
          </div>
        </CardContent>

        <CardActions className={classes.footer}>
          {!props.postPage ? (
            <Link
              className={classes.link}
              to={`/discussions/${props.discussionId}`}
            >
              Перейти к обсуждению
            </Link>
          ) : (
            !props.dateEnd && (
              <Link
                className={classes.link}
                to={`/map/?discussion=${props.discussionId}`}
              >
                Посмотреть обсуждение на карте
              </Link>
            )
          )}
        </CardActions>
      </Card>
    </>
  )
}

export default CardDiscussion
