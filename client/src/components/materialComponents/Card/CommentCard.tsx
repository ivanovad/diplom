import React, { useEffect } from 'react'
import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardHeader from '@material-ui/core/CardHeader'
import { commentStyles } from '../styles/comments'
import { cardStyles } from '../styles/card'
import userIcon from '../../../images/user.svg'
import { getName } from '../../../helpers/splitFunctions'
import { getTimeString } from '../../../helpers/dateTime'
import CustomMenu from '../Menu/CustomMenu'
import cx from 'classnames'

interface Props {
  text: string
  userEmail: string
  name: string
  surname: string
  date: string
  isAuth: boolean
  commentUserId: number
  curUserId: number | null
  commentId: number
  userType: string
}

function CommentCard(props: Props) {
  useEffect(() => {}, [props.isAuth])

  const commentClasses = commentStyles()
  const cardClasses = cardStyles()

  function isCommentOwner() {
    return props.curUserId === props.commentUserId
  }

  return (
    <div className={commentClasses.comment}>
      <div className={commentClasses.user}>
        <img src={userIcon} alt="user icon" />
        {isCommentOwner() ? (
          <div>Вы</div>
        ) : (
          <div>{getName(props.userEmail, props.name, props.surname)}</div>
        )}
      </div>

      <Card className={commentClasses.userComment}>
        {props.isAuth && (
          <CardHeader
            className={cardClasses.header}
            action={
              <CustomMenu
                iconType="24"
                type="comment"
                commentOwner={isCommentOwner()}
                commentId={props.commentId}
                userId={props.commentUserId}
                userType={props.userType}
              />
            }
          />
        )}
        <CardContent
          className={cx({ [commentClasses.commentContent]: props.isAuth })}
        >
          <div className={cardClasses.date}>{getTimeString(props.date)}</div>
          <div>{props.text}</div>
        </CardContent>
      </Card>
    </div>
  )
}

export default CommentCard
