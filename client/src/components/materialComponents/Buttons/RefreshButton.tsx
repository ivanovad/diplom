import React from 'react'
import Button from '@material-ui/core/Button'
import refresh from '../../../images/refresh.svg'
import { refreshButtonStyles } from '../styles/buttons'

function RefreshButton() {
  const classes = refreshButtonStyles()

  function reloadPage() {
    window.parent.location.reload()
  }

  return (
    <div>
      <Button
        variant="contained"
        className={classes.primary}
        startIcon={<img src={refresh} alt="refresh" />}
        onClick={reloadPage}
      >
        Обновить
      </Button>
    </div>
  )
}

export default RefreshButton
