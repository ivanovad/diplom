import React from 'react'
import { buttonStyles, ColorButton } from '../styles/buttons'
import cx from 'classnames'

interface Props {
  value: string;
  margin?: boolean;
  isSecondary?: boolean;
  mapForm?: boolean;
  onClick?: (event: any) => void;
  fullWidth?: boolean;
}

function CustomButton(props: Props) {
  const classes = buttonStyles()
  return (
    <div>
      <ColorButton
        className={cx({
          [classes.margin]: props.margin,
          [classes.secondary]: props.isSecondary,
          [classes.mapForm]: props.mapForm,
        })}
        fullWidth={props.fullWidth}
        onClick={props.onClick}
        variant="contained"
      >
        {props.value}
      </ColorButton>
    </div>
  )
}

export default CustomButton
