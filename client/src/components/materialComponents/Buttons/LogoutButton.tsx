import React from 'react'
import { ColorButtonLogout } from '../styles/buttons'

interface Props {
  value: string;
  header?: boolean;
  onClick?: (event: any) => void;
  fullWidth?: boolean;
}

function LogoutButton(props: Props) {
  return (
    <div>
      <ColorButtonLogout
        onClick={props.onClick}
        variant="contained"
        color="primary"
      >
        {props.value}
      </ColorButtonLogout>
    </div>
  )
}

export default LogoutButton
