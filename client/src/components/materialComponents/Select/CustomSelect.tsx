import React, { useState } from 'react'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import { selectStyles } from '../styles/select'

interface Props {
  setType: (type) => void
}

function CustomSelect(props: Props) {
  const classes = selectStyles()

  const [option, setOption] = useState('')

  const handleChange = (event) => {
    setOption(event.target.value)

    switch (event.target.value) {
      case '':
        props.setType('all')
        break
      case 'discussions':
        props.setType('discussions')
        break
      case 'comments':
        props.setType('comments')
        break
      default:
        break
    }
  }

  return (
    <div>
      <FormControl className={classes.formControl}>
        <InputLabel shrink>Типы жалоб</InputLabel>
        <Select
          value={option}
          onChange={handleChange}
          displayEmpty
          className={classes.selectEmpty}
        >
          <MenuItem value="">Все</MenuItem>
          <MenuItem value="discussions">Обсуждения</MenuItem>
          <MenuItem value="comments">Комментарии</MenuItem>
        </Select>
      </FormControl>
    </div>
  )
}

export default CustomSelect
