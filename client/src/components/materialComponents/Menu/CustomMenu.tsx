import React, { useState } from 'react'
import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import menu32 from '../../../images/menu32.svg'
import menu24 from '../../../images/menu24.svg'
import ConfirmDelete from '../Alerts/ConfirmDelete'
import ConfirmEnd from '../Alerts/ConfirmEnd'
import ComplaintDiscussion from '../Alerts/ComplaintDiscussion'
import ComplaintComment from '../Alerts/ComplaintComment'
import BlockUser from '../Alerts/BlockUser'

interface Props {
  iconType: '32' | '24'
  type: 'discussion' | 'comment' | 'complaint'
  commentOwner?: boolean
  discussionOwner?: boolean
  commentId?: number
  discussionId?: number
  complaintId?: number
  isEnd?: boolean
  userId?: number
  authorId?: number
  userType: string
}

function CustomMenu(props: Props) {
  const [anchorEl, setAnchorEl] = useState(null)
  const [deleteType, setDeleteType] = useState('')
  const [type, setType] = useState('author')
  const [openConfirm, setOpenConfirm] = useState(false)
  const [openConfirmDelete, setOpenConfirmDelete] = useState(false)
  const [openComplaintDiscussion, setOpenComplaintDiscussion] = useState(false)
  const [openComplaintComment, setOpenComplaintComment] = useState(false)
  const [openBlockUser, setOpenBlockUser] = useState(false)

  const open = Boolean(anchorEl)

  let options: Array<string> = []

  if (props.userType === 'admin' && props.type !== 'complaint') {
    if (props.discussionId !== undefined) {
      options.push('Удалить обсуждение')

      if (!props.discussionOwner) {
        options.push('Пожаловаться')
        options.push('Заблокировать пользователя')
      }

      if (!props.isEnd) {
        options.push('Завершить')
      }
    } else if (props.commentId !== undefined) {
      options.push('Удалить комментарий')

      if (!props.commentOwner) {
        options.push('Пожаловаться')
        options.push('Заблокировать пользователя')
      }
    }
  } else if (props.userType !== 'admin') {
    if (props.discussionOwner) {
      options.push('Удалить обсуждение')

      if (!props.isEnd) {
        options.push('Завершить')
      }
    } else if (props.commentOwner) {
      options.push('Удалить комментарий')
    } else {
      options.push('Пожаловаться')
    }
  } else {
    options = ['Удалить жалобу', 'Заблокировать нарушающего пользователя']
    if (props.commentId) {
      options.push('Удалить комментарий')
    } else {
      options.push('Удалить обсуждение')
      if (!props.isEnd) {
        options.push('Завершить')
      }
    }
    options.push('Заблокировать автора жалобы')
  }

  function handleClick(event) {
    setAnchorEl(event.currentTarget)
  }

  function handleClose(event) {
    setAnchorEl(null)

    switch (event.target.dataset.key) {
      case 'Удалить комментарий':
        setOpenConfirmDelete(true)
        setDeleteType('comment')
        break
      case 'Удалить обсуждение':
        setOpenConfirmDelete(true)
        setDeleteType('discussion')
        break
      case 'Удалить жалобу':
        setOpenConfirmDelete(true)
        setDeleteType('complaint')
        break
      case 'Завершить':
        setOpenConfirm(true)
        break
      case 'Заблокировать нарушающего пользователя':
        setOpenBlockUser(true)
        break
      case 'Заблокировать автора жалобы':
        setType('user')
        setOpenBlockUser(true)
        break
      case 'Заблокировать пользователя':
        setType('user')
        setOpenBlockUser(true)
        break
      case 'Пожаловаться':
        if (props.discussionId) {
          setOpenComplaintDiscussion(true)
        } else {
          setOpenComplaintComment(true)
        }
        break
      default:
        break
    }
  }

  return (
    <div>
      {options.length !== 0 && (
        <>
          <IconButton
            aria-label="more"
            aria-haspopup="true"
            onClick={handleClick}
          >
            {props.iconType === '32' ? (
              <img src={menu32} alt="menu icon" />
            ) : (
              <img src={menu24} alt="menu icon" />
            )}
          </IconButton>
          <Menu
            keepMounted
            open={open}
            anchorEl={anchorEl}
            onClose={handleClose}
          >
            {options.map((option) => (
              <MenuItem key={option} data-key={option} onClick={handleClose}>
                {option}
              </MenuItem>
            ))}
          </Menu>
        </>
      )}

      <ConfirmDelete
        openConfirmDelete={openConfirmDelete}
        setOpenConfirmDelete={setOpenConfirmDelete}
        commentId={props.commentId}
        discussionId={props.discussionId}
        complaintId={props.complaintId}
        type={deleteType}
      />

      <ConfirmEnd
        discussionId={props.discussionId}
        openConfirm={openConfirm}
        setOpenConfirm={setOpenConfirm}
      />

      <ComplaintDiscussion
        discussionId={props.discussionId}
        openComplaintDiscussion={openComplaintDiscussion}
        setOpenComplaintDiscussion={setOpenComplaintDiscussion}
      />

      <ComplaintComment
        commentId={props.commentId}
        openComplaintComment={openComplaintComment}
        setOpenComplaintComment={setOpenComplaintComment}
      />

      <BlockUser
        userId={props.userId}
        authorId={props.authorId}
        type={type}
        setOpenBlockUser={setOpenBlockUser}
        openBlockUser={openBlockUser}
      />
    </div>
  )
}

export default CustomMenu
