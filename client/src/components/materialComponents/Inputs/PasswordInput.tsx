import React, { ChangeEvent, useState } from 'react'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import { formStyles } from '../styles/inputs'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton'
import Visibility from '../../../images/eye.svg'
import VisibilityOff from '../../../images/eye-crossed.svg'
import { FormHelperText } from '@material-ui/core'

interface Props {
  value: string;
  label: string;
  id: string;
  error?: null | string;
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
}

function PasswordInput(props: Props) {
  const [showPassword, setShowPassword] = useState(false)

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword)
  }

  const classes = formStyles()
  return (
    <div>
      <FormControl className={classes.margin} variant="outlined" fullWidth>
        <InputLabel required htmlFor={props.id} error={Boolean(props.error)}>
          {props.label}
        </InputLabel>
        <OutlinedInput
          id={props.id}
          type={showPassword ? 'text' : 'password'}
          value={props.value}
          onChange={props.handleChange}
          endAdornment={
            <InputAdornment position="end">
              <IconButton
                aria-label="toggle password visibility"
                onClick={handleClickShowPassword}
              >
                {showPassword ? (
                  <img src={Visibility} alt="eye" />
                ) : (
                  <img src={VisibilityOff} alt="eye closed" />
                )}
              </IconButton>
            </InputAdornment>
          }
          labelWidth={props.id === 'passwordRepeat' ? 150 : 70}
          error={Boolean(props.error)}
        />

        <FormHelperText error={Boolean(props.error)}>
          {props.error}
        </FormHelperText>
      </FormControl>
    </div>
  )
}

export default PasswordInput
