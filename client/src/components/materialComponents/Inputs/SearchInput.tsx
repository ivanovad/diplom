import React, { useState } from 'react'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import IconButton from '@material-ui/core/IconButton'
import searchIcon from '../../../images/search.svg'
import { searchStyles } from '../styles/searchInput'
import { PostData } from '../../PostsList'
import Api from '../../../api/DiscussionsApi'
import Alerts from '../Alerts/Alerts'

interface Props {
  setPostsList: (posts: PostData[]) => void
  setIsSearch: (isSearch: boolean) => void
}

function SearchInput(props: Props) {
  const classes = searchStyles()
  const [value, setValue] = useState('')
  const [open, setOpen] = useState(false)
  const [response, setResponse] = useState('')

  function handleChange(event) {
    setValue(event.target.value)
  }

  async function handleClick() {
    const result = await Api.findPosts(value)

    if (result.status === 0 || result.status === 1) {
      props.setPostsList(result.resultData)
      props.setIsSearch(true)
    } else {
      setResponse(result.message)
      setOpen(true)
    }
  }

  function handleKeyPress(event) {
    if (event.key === 'Enter') {
      handleClick()
    }
  }

  return (
    <div className={classes.container}>
      <Paper className={classes.root} onKeyPress={handleKeyPress}>
        <InputBase
          className={classes.input}
          placeholder="Введите поисковый запрос"
          inputProps={{ 'aria-label': 'search' }}
          value={value}
          onChange={handleChange}
        />

        <IconButton
          className={classes.iconButton}
          aria-label="search"
          onClick={handleClick}
        >
          <img src={searchIcon} alt="Поиск" />
        </IconButton>
      </Paper>

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="posts"
        response={response}
      />
    </div>
  )
}

export default SearchInput
