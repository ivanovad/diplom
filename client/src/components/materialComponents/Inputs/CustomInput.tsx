import React, { ChangeEvent } from 'react'
import OutlinedInput from '@material-ui/core/OutlinedInput'
import InputLabel from '@material-ui/core/InputLabel'
import FormControl from '@material-ui/core/FormControl'
import { formStyles } from '../styles/inputs'
import { FormHelperText } from '@material-ui/core'
import cx from 'classnames'

interface Props {
  label: string;
  value: string;
  id: string;
  handleChange: (event: ChangeEvent<HTMLInputElement>) => void;
  required?: boolean;
  error?: null | string;
  map?: boolean;
  limit?: number;
  multiline?: boolean;
}

function CustomInput(props: Props) {
  const classes = formStyles()

  return (
    <div>
      <FormControl
        className={cx(classes.margin, { [classes.map]: props.map })}
        fullWidth
        variant="outlined"
      >
        <InputLabel
          error={Boolean(props.error)}
          required={props.required}
          htmlFor={props.id}
        >
          {props.label}
        </InputLabel>
        <OutlinedInput
          id={props.id}
          label={props.label}
          value={props.value}
          onChange={props.handleChange}
          multiline={props.multiline}
          rows={5}
          labelWidth={80}
          error={Boolean(props.error)}
          inputProps={{ maxLength: props.limit }}
        />

        {!props.error && props.limit && (
          <FormHelperText>{`${props.value.length}/${props.limit}`}</FormHelperText>
        )}

        <FormHelperText error={Boolean(props.error)}>
          {props.error}
        </FormHelperText>
      </FormControl>
    </div>
  )
}

export default CustomInput
