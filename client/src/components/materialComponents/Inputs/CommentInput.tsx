import React, { useState } from 'react'
import Paper from '@material-ui/core/Paper'
import InputBase from '@material-ui/core/InputBase'
import { commentStyles } from '../styles/comments'
import userIcon from '../../../images/user.svg'
import CustomButton from '../Buttons/CustomButton'
import Api from '../../../api/CommentsApi'
import Alerts from '../Alerts/Alerts'
import * as validation from '../../../helpers/validation/formsValidation'
import FormHelperText from '@material-ui/core/FormHelperText'

interface Props {
  discussionId: number
}

function CommentInput(props: Props) {
  const classes = commentStyles()
  const [response, setResponse] = useState('')
  const [open, setOpen] = useState(false)
  const [value, setValue] = useState('')
  const [error, setError] = useState(false)

  async function handleSendClick() {
    const noExtraSpacesText = validation.getNoExtraSpacesText(value)

    if (!noExtraSpacesText) {
      setError(true)
    } else {
      const result = await Api.setComment(props.discussionId, noExtraSpacesText)

      if (result.status !== 0) {
        setResponse(result.message)
        setOpen(true)
      }
    }
  }

  function handleChange(event) {
    setValue(event.target.value)

    if (event.target.value) {
      setError(false)
    }
  }

  return (
    <div className={classes.container}>
      <img className={classes.img} src={userIcon} alt="user icon" />
      <div className={classes.textContainer}>
        <Paper component="form" className={classes.root}>
          <InputBase
            className={classes.input}
            placeholder="Напишите комментарий"
            inputProps={{ 'aria-label': 'comment', maxLength: 200 }}
            multiline
            rows={3}
            rowsMax={4}
            value={value}
            onChange={handleChange}
          />
        </Paper>
        <FormHelperText error={error}>
          {error ? 'Поле не может быть пустым' : `${value.length}/${200}`}
        </FormHelperText>

        <CustomButton value="Отправить" onClick={handleSendClick} />
      </div>

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="comment"
        response={response}
      />
    </div>
  )
}

export default CommentInput
