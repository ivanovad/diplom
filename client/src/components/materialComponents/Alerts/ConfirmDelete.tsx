import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Alerts from './Alerts'
import Api from '../../../api/CommentsApi'
import DiscussionsApi from '../../../api/DiscussionsApi'
import ComplaintsApi from '../../../api/ComplaintsApi'

interface Props {
  commentId: number | undefined
  discussionId: number | undefined
  complaintId: number
  type: string
  openConfirmDelete: boolean
  setOpenConfirmDelete: React.Dispatch<React.SetStateAction<boolean>>
}

function ConfirmDelete(props: Props) {
  const [response, setResponseMessage] = useState('')
  const [openError, setOpen] = useState(false)

  async function deleteComment() {
    const result = await Api.deleteComment(props.commentId)

    setResponseMessage(result.message)
    setOpen(true)
  }

  async function deleteDiscussion() {
    const result = await DiscussionsApi.deleteDiscussion(props.discussionId)

    setResponseMessage(result.message)
    setOpen(true)

    if (result.status === 0) {
      setTimeout(() => window.parent.location.reload(), 1500)
    }
  }

  async function deleteComplaint() {
    const result = await ComplaintsApi.deleteComplaint(
      props.complaintId,
      props.commentId
    )

    setResponseMessage(result.message)
    setOpen(true)

    if (result.status === 0) {
      setTimeout(() => window.parent.location.reload(), 1500)
    }
  }

  function handleYes() {
    props.setOpenConfirmDelete(false)
    if (props.type === 'comment') {
      deleteComment()
    } else if (props.type === 'discussion') {
      deleteDiscussion()
    } else {
      deleteComplaint()
    }
  }

  function handleClose() {
    props.setOpenConfirmDelete(false)
  }

  return (
    <div>
      <Dialog
        open={props.openConfirmDelete}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Подтверждение удаления
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            {`Вы действительно хотите удалить ${
              props.type === 'comment'
                ? 'комментарий'
                : props.type === 'discussion'
                ? 'обсуждение'
                : 'жалобу'
            }?`}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={handleYes} color="primary" autoFocus>
            Удалить
          </Button>
        </DialogActions>
      </Dialog>

      <Alerts
        open={openError}
        autoHideDuration={5000}
        setOpen={setOpen}
        type="menu"
        response={response}
      />
    </div>
  )
}

export default ConfirmDelete
