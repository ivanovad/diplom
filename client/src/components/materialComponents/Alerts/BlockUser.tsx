import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Alerts from './Alerts'
import FormHelperText from '@material-ui/core/FormHelperText'
import Api from '../../../api/UserApi'
import { getNoExtraSpacesText } from '../../../helpers/validation/formsValidation'

interface Props {
  userId: number | undefined
  authorId: number | undefined
  type: string
  openBlockUser: boolean
  setOpenBlockUser: React.Dispatch<React.SetStateAction<boolean>>
}

function BlockUser(props: Props) {
  const [response, setResponseMessage] = useState('')
  const [openError, setOpen] = useState(false)
  const [value, setValue] = useState('')
  const [valueError, setValueError] = useState(false)

  const handleClose = () => {
    props.setOpenBlockUser(false)
  }

  async function handleSend() {
    const noExtraSpacesText = getNoExtraSpacesText(value)
    if (!noExtraSpacesText) {
      setValueError(true)
    } else {
      console.log(props.type, props.userId, props.authorId)
      const result = await Api.blockUser(
        props.type === 'user' ? props.userId : props.authorId,
        noExtraSpacesText
      )
      setResponseMessage(result.message)
      setOpen(true)
      if (result.status === 0) {
        props.setOpenBlockUser(false)
      }
    }
  }

  function handleChange(event) {
    setValue(event.target.value)

    if (event.target.value) {
      setValueError(false)
    }
  }

  return (
    <div>
      <Dialog
        open={props.openBlockUser}
        onClose={handleClose}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          Блокировка пользователя
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            Введите причину блокировки в поле ниже
          </DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="reason"
            label="Причина блокировки"
            type="block"
            value={value}
            onChange={handleChange}
            inputProps={{ maxLength: 60 }}
            fullWidth
          />

          <FormHelperText error={valueError}>
            {valueError ? 'Поле не может быть пустым' : `${value.length}/${60}`}
          </FormHelperText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={handleSend} color="primary">
            Отправить
          </Button>
        </DialogActions>
      </Dialog>

      <Alerts
        open={openError}
        autoHideDuration={5000}
        setOpen={setOpen}
        type="block"
        response={response}
      />
    </div>
  )
}

export default BlockUser
