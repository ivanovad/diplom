import React from 'react'
import Dialog from '@material-ui/core/Dialog'
import Button from '@material-ui/core/Button'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'

interface Props {
  reason: string
  setOpenBlock: (open) => void
  openBlock: boolean
}

function BlockAlert(props: Props) {
  const handleClose = () => {
    props.setOpenBlock(false)
  }

  return (
    <div>
      <Dialog
        open={props.openBlock}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">Вы заблокированы</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            <div>Вы были заблокированы модератором по причине:</div>
            <div>{props.reason}</div>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary" autoFocus>
            Закрыть
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

export default BlockAlert
