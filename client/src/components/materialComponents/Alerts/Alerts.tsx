import React from 'react'
import { Snackbar } from '@material-ui/core'
import MuiAlert from '@material-ui/lab/Alert'

interface Props {
  open: boolean
  response: string | null
  setOpen: (open: boolean) => void
  type: string
  autoHideDuration?: number
}

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />
}

function Alerts(props: Props) {
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return
    }

    props.setOpen(false)
  }
  let severityText = 'success'
  let value = props.response

  switch (props.type) {
    case 'login':
      severityText = 'error'
      if (props.response === 'Пользователь не найден') {
        value = 'Неверный логин или пароль'
      }

      break
    case 'register':
      if (props.response === 'Пользователь добавлен') {
        value = 'Вы успешно зарегистрировались'
      } else if (props.response === 'Администратор добавлен') {
        value = 'Модератор успешно добавлен'
      } else {
        severityText = 'error'
      }
      break
    case 'map':
      if (props.response === 'Обсуждение добавлено') {
        value = 'Метка и обсуждение созданы'
      } else if (props.response === 'Пока нет ни одной метки') {
        severityText = 'info'
      } else {
        severityText = 'error'
      }
      break
    case 'complaint':
      if (props.response === 'Жалоба оставлена') {
        value = 'Жалоба успешно оставлена'
      } else {
        severityText = 'error'
      }
      break
    case 'block':
      if (props.response === 'Пользователь заблокирован') {
        value = 'Пользователь успешно заблокирован'
      } else {
        severityText = 'error'
      }
      break
    case 'menu':
      if (
        props.response === 'Комментарий удалён' ||
        props.response === 'Обсуждение удалено' ||
        props.response === 'Жалоба удалена'
      ) {
        value = props.response
      } else {
        severityText = 'error'
      }
      break
    default:
      severityText = 'error'
      break
  }

  return (
    <Snackbar
      open={props.open}
      onClose={handleClose}
      autoHideDuration={props.autoHideDuration}
    >
      <Alert onClose={handleClose} severity={severityText}>
        {value}
      </Alert>
    </Snackbar>
  )
}

export default Alerts
