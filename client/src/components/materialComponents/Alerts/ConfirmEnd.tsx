import React, { useState } from 'react'
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Alerts from './Alerts'
import Api from '../../../api/DiscussionsApi'

interface Props {
  discussionId: number | undefined
  openConfirm: boolean
  setOpenConfirm: React.Dispatch<React.SetStateAction<boolean>>
}

function ConfirmEnd(props: Props) {
  const [response, setResponseMessage] = useState('')
  const [openError, setOpen] = useState(false)

  async function deleteComment() {
    const result = await Api.endDiscussion(props.discussionId)

    if (result.status !== 0) {
      setResponseMessage(result.message)
      setOpen(true)
    } else {
      window.parent.location.reload()
    }
  }

  function handleYes() {
    props.setOpenConfirm(false)
    deleteComment()
  }

  function handleClose() {
    props.setOpenConfirm(false)
  }

  return (
    <div>
      <Dialog
        open={props.openConfirm}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">
          Подтверждение завершения
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Вы действительно хотите завершить обсуждение?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Отмена
          </Button>
          <Button onClick={handleYes} color="primary" autoFocus>
            Завершить
          </Button>
        </DialogActions>
      </Dialog>

      <Alerts
        open={openError}
        autoHideDuration={5000}
        setOpen={setOpen}
        type="menu"
        response={response}
      />
    </div>
  )
}

export default ConfirmEnd
