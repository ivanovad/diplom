import Button from '@material-ui/core/Button'
import React, { useEffect, useState } from 'react'
import '../styles/App.css'
import Api from '../api/DiscussionsApi'
import Alerts from './materialComponents/Alerts/Alerts'
import RefreshButton from './materialComponents/Buttons/RefreshButton'
import CardDiscussion from './materialComponents/Card/CardDiscussion'
import {
  buttonStyles,
  refreshButtonStyles,
} from './materialComponents/styles/buttons'
import { PostData } from './PostsList'

interface Props {
  isAuth: boolean
  userType: string
}

export default function UserPosts(props: Props) {
  const [open, setOpen] = useState(false)
  const [discussions, setDiscussions] = useState<PostData[]>([])
  const [response, setResponse] = useState('')
  const [isLoading, setIsLoading] = useState(true)
  const [showMore, setShowMore] = useState(1)

  useEffect(() => {
    async function getUserPosts() {
      const result = await Api.getPosts(true)

      if (result.status === 0) {
        setDiscussions(result.resultData)
      } else if (result.status === 1) {
        setResponse(result.message)
      } else {
        setResponse(result.message)
        if (result.message !== 'Пользователь не авторизован') {
          setOpen(true)
        }
      }

      setIsLoading(false)
    }
    getUserPosts()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {}, [props.isAuth, props.userType])

  const classes = refreshButtonStyles()
  const buttonClasses = buttonStyles()

  return (
    <div>
      {!props.isAuth && (
        <div className="not-content">
          Авторизуйтесь, чтобы оставлять метки и создавать обсуждения
        </div>
      )}

      {props.isAuth &&
        !isLoading &&
        (discussions.length === 0 &&
        response !== 'Пользователь не авторизован' ? (
          <>
            <div className={classes.container}>
              <RefreshButton />
            </div>
            <div className="not-content">
              Вы пока не создали ни одного обсуждения
            </div>
          </>
        ) : response === 'Пользователь не авторизован' ? (
          <div className="not-content">
            Авторизуйтесь, чтобы оставлять метки и создавать обсуждения
          </div>
        ) : (
          <>
            <div className={classes.container}>
              <RefreshButton />
            </div>

            {discussions.slice(0, showMore * 4).map((post) => (
              <CardDiscussion
                key={`post-${post.discussion_id}`}
                title={post.title}
                content={post.discussion_text}
                userEmail={post.email}
                dateStart={post.date_start}
                dateEnd={post.date_end}
                name={post.name}
                surname={post.surname}
                location={post.object}
                discussionId={post.discussion_id}
                isAuth={props.isAuth}
                userType={props.userType}
              />
            ))}

            {showMore * 4 < discussions.length && (
              <Button
                fullWidth
                className={buttonClasses.primary}
                variant="contained"
                onClick={() => setShowMore(showMore + 1)}
              >
                Показать больше
              </Button>
            )}
          </>
        ))}

      <Alerts
        open={open}
        autoHideDuration={3000}
        setOpen={setOpen}
        type="posts"
        response={response}
      />
    </div>
  )
}
