-- Перезапись схемы citizen
DROP SCHEMA IF EXISTS citizen CASCADE;
CREATE SCHEMA citizen;

-- Создание домена (типа данных с доп. условиями) для типа пользователя
CREATE DOMAIN citizen.user_types AS character(5) CHECK(VALUE in ('user', 'admin'));
-- Создание таблицы для пользователей
CREATE TABLE citizen.users (
user_id SERIAL PRIMARY KEY,
email character(254) NOT NULL UNIQUE,
password character(72) NOT NULL,
name character(31),
surname character(43),
type citizen.user_types DEFAULT 'user');

-- Создание таблицы с координатами обсуждений
CREATE TABLE citizen.discussions_coords (
coords_id SERIAL PRIMARY KEY,
latitude double precision NOT NULL,
longitude double precision NOT NULL,
UNIQUE(latitude, logitude));

-- Создание таблицы с обсуждениями
CREATE TABLE citizen.discussions (
discussion_id SERIAL PRIMARY KEY,
user_id SERIAL,
coords_id SERIAL,
title character(50) NOT NULL,
discussion_text text,
date_end timestamp,
date_start timestamp NOT NULL DEFAULT LOCALTIMESTAMP(0),
FOREIGN KEY (user_id) REFERENCES citizen.users(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (coords_id) REFERENCES citizen.discussions_coords(coords_id) ON DELETE CASCADE ON UPDATE CASCADE,
UNIQUE(user_id, coords_id, title));

-- Создание таблицы с адресами, связанными с координатами
CREATE TABLE citizen.coords_objects (
coords_obj_id SERIAL PRIMARY KEY,
coords_id SERIAL,
object text NOT NULL,
FOREIGN KEY (coords_id) REFERENCES citizen.discussions_coords(coords_id) ON DELETE CASCADE ON UPDATE CASCADE,
UNIQUE(coords_id, object));

-- Создание таблицы с комментариями
CREATE TABLE citizen.comments (
comment_id SERIAL PRIMARY KEY,
user_id SERIAL,
discussion_id SERIAL,
content text NOT NULL,
date timestamp NOT NULL DEFAULT LOCALTIMESTAMP(0),
FOREIGN KEY (user_id) REFERENCES citizen.users(user_id) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (discussion_id) REFERENCES citizen.discussions(discussion_id) ON DELETE CASCADE ON UPDATE CASCADE);

-- Создание таблицы с жалобами на обсуждения
CREATE TABLE citizen.discussions_complaints (
complaint_id SERIAL PRIMARY KEY,
discussion_id SERIAL,
user_id SERIAL,
reason text NOT NULL,
FOREIGN KEY (discussion_id) REFERENCES citizen.discussions(discussion_id) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (user_id) REFERENCES citizen.users(user_id) ON DELETE CASCADE ON UPDATE CASCADE);

-- Создание таблицы с жалобами на комментарии
CREATE TABLE citizen.comments_complaints (
complaint_id SERIAL PRIMARY KEY,
comment_id SERIAL,
user_id SERIAL,
reason text NOT NULL,
FOREIGN KEY (comment_id) REFERENCES citizen.comments(comment_id) ON DELETE CASCADE ON UPDATE CASCADE,
FOREIGN KEY (user_id) REFERENCES citizen.users(user_id) ON DELETE CASCADE ON UPDATE CASCADE);

-- Создание таблицы заблокированных пользователей
CREATE TABLE citizen.users_blocks (
user_blocks_id SERIAL PRIMARY KEY,
user_id SERIAL,
reason text NOT NULL,
FOREIGN KEY (user_id) REFERENCES citizen.users(user_id) ON DELETE CASCADE ON UPDATE CASCADE);

-- Создание таблицы для хранения сессий
CREATE TABLE citizen.session (
  sid varchar NOT NULL COLLATE "default",
	sess json NOT NULL,
	expire timestamp(6) NOT NULL
)
WITH (OIDS=FALSE);
ALTER TABLE citizen.session ADD CONSTRAINT "session_pkey" PRIMARY KEY ("sid") NOT DEFERRABLE INITIALLY IMMEDIATE;
CREATE INDEX "IDX_session_expire" ON citizen.session("expire");